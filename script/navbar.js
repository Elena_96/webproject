var isDesktop
var buttonPressed;
var imgButton = "../img/x.png";
var dimensionImg = "30px";
var lastBtnPressed = 0;
var menu=0;

function change(btn, img) {
  imgButton = $(btn).attr("src");
  $(btn).animate({height: "0px"}, 100, function(){$(btn).attr('src',img);});
  $(btn).animate({height: dimensionImg},100);
};

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
function openNav() {
    document.getElementById("mySidenav").style.width = "280px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function openClose() {
  if (menu===0) {
    if (lastBtnPressed!==0) {
      $(lastBtnPressed).trigger("click");
    }
    menu=1;
    openNav();
  } else {
    menu=0;
    closeNav();
  }
  event.stopPropagation();
}

$(document).ready(function(){
  if ($(window).width()<1200) {
    isDesktop=0;
    $("#navbar-desktop").hide();
  } else {
    isDesktop=1;
    $("#navbar-mobile").hide();

  }
  $(window).resize(function() {
    if($(this).width()<1200 && isDesktop) {
      isDesktop=0;
      $("#navbar-mobile").fadeToggle();
      $("#navbar-desktop").fadeToggle();
    } if ($(this).width()>=1200 && !isDesktop) {
      isDesktop=1;
      $("#navbar-mobile").fadeToggle();
      $("#navbar-desktop").fadeToggle();
    }
  });

  $('nav>div>button').click(function(){
    buttonPressed = $(this).find('img');
    //dimensionImg = $(buttonPressed).css("height");
    if (this === lastBtnPressed || lastBtnPressed === 0) {
      change(buttonPressed, imgButton);
      if (lastBtnPressed === 0) {
        lastBtnPressed = this;
      } else {
        lastBtnPressed = 0;
      }
    } else {
      $(lastBtnPressed).trigger("click");
      //change($(lastBtnPressed).find('img'), imgButton);
      $(this).trigger("click");
    }
  });
  $('nav>ul>li>button').click(function(){
    buttonPressed = $(this).find('img');
    //dimensionImg = $(buttonPressed).css("height");
    if (this === lastBtnPressed || lastBtnPressed === 0) {
      change(buttonPressed, imgButton);
      if (lastBtnPressed === 0) {
        lastBtnPressed = this;
      } else {
        lastBtnPressed = 0;
      }
    } else {
      $(lastBtnPressed).trigger("click");
      //change($(lastBtnPressed).find('img'), imgButton);
      $(this).trigger("click");
    }
  });
  $('#carrello i').click(function(){
    var itemId=this.getAttribute('id');
    var tipo=itemId.split("-")[0];
    var id=itemId.split("-")[1];
    var select="#";
    select=select.concat("",itemId);
    $.ajax({ url: 'deleteCarrello.php',
         data: {action: 'delete', tipologia: tipo , id: id },
         type: 'post',
         success: function(output) {
                      if (output) {
                        $(select).slideUp();
                        var prezzo = $(select.concat("",">p")).text();
                        prezzo=prezzo.replace("€","");
                        var tot = $('.tot').text();
                        ntot=tot.split(" ")[2].replace("€","");
                        newtot=ntot-prezzo;
                        newtot=newtot.toFixed(2);
                        if (newtot==="0.00") {
                          $('#carrello>section:last-child').slideUp();
                          $('#carrello>section>h1').text("Carrello vuoto");
                        } else {
                          $('.tot').text(tot.replace(ntot,newtot));
                        }
                      };
                  }
    });
  });
  $('#notifiche i').click(function(){
    var id=this.getAttribute('id');
    var select="#";
    select=select.concat("",id);
    $.ajax({ url: 'viewNotify.php',
         data: {action: 'view', id: id },
         type: 'post',
         success: function(output) {
                      if (output) {
                        $(select).slideUp();
                      };
                  }
    });
  });

  $(window).click(function () {
    if(menu == 1) {
      closeNav();
    }
  });
});
