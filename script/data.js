$(document).ready(function(){
  var elem = document.querySelector('.switch');
  var init = new Switchery(elem);
  $(".password").slideUp();
  elem.onchange = function() {
    $(".password").slideToggle();
  };
});
