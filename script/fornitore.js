$(document).ready(function(){

  $("#menu button").click(function() {
    showElement($(this).attr("id").substr(0));
  });
  showElement("dash");
  updateNotifiche();
  setInterval(updateNotifiche(), 3000);
});

function updateNotifiche() {
  if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
  } else {
      // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          $("#badge").html(this.responseText);
      }
  };
  xmlhttp.open("GET","./func_page/badgeNotifiche.php",true);
  xmlhttp.send();
}

function showElement(id) {
  var pagina = "";
  switch (id) {
    case "dash":
      pagina = "dashboardFornitore.php";
      break;
    case "ordini":
      pagina = "dashboardOrdini.php";
      break;
    case "promo":
      pagina = "aggiungiPromo.php";
      break;
    default:
      pagina = "aggiungiElemento.php?q=" + id.substr(4);
      break;
  }
  if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
  } else {
      // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          $("#dati").html(this.responseText);
      }
  };
  xmlhttp.open("GET",pagina,true);
  xmlhttp.send();
}
