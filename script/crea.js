﻿var panino;

function Panino() {
  this.ingr = [];
  this.totale = 0.0;

  this.aumentaTotale = function(prezzo) {
    this.totale += Number(prezzo);
  }

  this.abbassaTotale = function(prezzo) {
    this.totale -= Number(prezzo);
    if (this.totale < 0) {
      this.totale = 0.0;
    }
  }

  /**
   * aggiungi un ingrediente al panino; ritorna:
   * - (-1) se si sta inserendo un ingrediente e non è ancora presente il pane
   * -  (0) se è già presente un ingrediente diverso ma dello stesso tipo
   * -  (1) se l'inserimento è avvenuto correttamente
   **/
  this.aggiungiIngr = function(id, tipo, prezzo) {
    if (tipo !== 1 && notExists(this.ingr[1])) {
      return -1;
    } else if(tipo !== 5) {
      if(notExists(this.ingr[tipo])) {
        this.ingr[tipo] = [id, 1];
      } else if (tipo !== 1 && this.ingr[tipo][0].localeCompare(id) == 0) {
        this.ingr[tipo][1]++;
      } else {
        return 0;
      }
    }
    this.aumentaTotale(prezzo);
    return 1;
  }

  /**
   * aggiungi un ingrediente al panino; ritorna:
   * - (-1) se si sta eliminando un ingrediente non presente
   * -  (0) se si è eliminato il pane
   * -  (1) se l'eliminazione è avvenuta correttamente
   **/
  this.rimuoviIngr = function(tipo, prezzo) {
    if (notExists(this.ingr[tipo])) {
      return -1;
    } else if(tipo !== 5) {
      this.ingr[tipo][1]--;
      if(this.ingr[tipo][1] === 0) {
        delete this.ingr[tipo];
      }
    }
    if (tipo === 1) {
      this.totale = 0.0;
      for (var i = 0; i < this.ingr.length; i++) {
        delete this.ingr[i];
      }
      return 0;
    } else {
      this.abbassaTotale(prezzo);
      return 1;
    }
  }

  this.getTotale = function() {
    return this.totale;
  }

  this.isOk = function(){
    return !notExists(this.ingr[1]) && !notExists(this.ingr[2]);
  }
}

$(document).ready(function () {
  panino = new Panino();
});

function inserisci(id,tipo,img_id) {
  var immagine = $(id + ">div:first-child>img").attr("src");
  var img = "<img id='" + img_id + "' src='" + immagine + "' alt='" + panino + "' class='img-fluid'>";
  $("#panino-scelto>#costruisci").append(img);
  if (tipo === 1) {
    var immagine = "<img src='../img/immagini/sopra.png' alt='' class='img-fluid' style='order: 99;'>";
    var luogo = $("#panino-scelto>#costruisci").append(immagine);
  }
}

function aggiungiElemento(id, idProd, codTipo, nome, prezzo) {
  var prezzoReale = prezzo.substring(0,prezzo.length-1);
  var count = Number($("#cart input[name='count']").val()) + 1;
  var tab_id = "r_" + count;
  var i_id = "i_" + count;
  var img_id = "p_" + count;
  var newRow = "<tr id='" + tab_id + "'>" +
                 "<td headers='comp' class='container-fluid'>" +
                   "<div class='row'>" +
                     "<em class='fa fa-minus-circle' style='align-self:center;' onclick='rimuovi(" + tab_id + "," + i_id + "," + img_id + "," + codTipo + "," + prezzoReale + ")'></em>" +
                     "<div class='col-10 col-sm-11'>" + nome + "</div>" +
                   "</div>" +
                 "</td>" +
                 "<td headers='prezzo'>" + prezzo + "</td>" +
               "</tr>";
  var totale = panino.getTotale().toFixed(2).toString() + "€";
  if ( count === 1) {
    $("#vuoto").remove();
    $("#tabella > tbody").append(newRow);
  } else {
    $("#tabella > tbody > tr:last-child").after(newRow);
  }
  $("#tabella tfoot td").html(totale);
  var count = Number($("#cart input[name='count']").val()) + 1;
  $("#cart input[name='count']").val(count);
  var newInput = "<input type='hidden' id='" + i_id + "' name='ing" + count + "' value='" + idProd + "_" + codTipo + "'>";
  $("#cart").append(newInput);
  $("#cart input[name='totale']").val(panino.getTotale().toFixed(2));
  return img_id;
}

function elaboraAggiunta(id, idProd, codTipo, nome, prezzo, ris, aggiunta) {
  if(ris === -1) {
    errorOccured("Non hai ancora selezionato un tipo di pane!");
  } else if (ris === 0) {
    if(codTipo === 1) {
      errorOccured("Hai già scelto un tipo di pane!");
    } else {
      errorOccured("Non puoi inserire due tipi diversi di " + aggiunta + "!");
    }
  } else {
    var img_id = aggiungiElemento(id, idProd, codTipo, nome, prezzo);
    inserisci(id, codTipo, img_id);
  }
}

function aggiungi(id, idProd, codTipo, tipo) {
  var nome = $(id + ">.dettagli>h2").html();
  var prezzo = $(id + ">.aggiungi>span").html();
  var ris = panino.aggiungiIngr(id, codTipo, prezzo.substring(0,prezzo.length-1));
  elaboraAggiunta(id, idProd, codTipo, nome, prezzo, ris, tipo.toLowerCase());
}

function rimuovi(row, i_row, img_id, codTipo, prezzo) {
  var ris = panino.rimuoviIngr(codTipo,prezzo);
  var count;
  if (ris === 0) {
    $("#tabella tbody tr").remove();
    $("#costruisci img").remove();
    $("#cart input").each(function(i) {
      if ($(this).attr("name").localeCompare("totale") !== 0 &&
          $(this).attr("name").localeCompare("count") !== 0) {
        $(this).remove();
      }
    });
    count = 0;
  } else {
    $(row).remove();
    $(i_row).remove();
    $(img_id).remove();
    var last_count = $("#cart input:last-child").attr("name");
    count = last_count.substring(last_count.length - 1);
  }
  var totale = panino.getTotale().toFixed(2).toString() + "€";
  $("#tabella tfoot td").html(totale);
  $("#cart input[name='count']").val(count);
  $("#cart input[name='totale']").val(panino.getTotale().toFixed(2));
  if(count == 0) {
    var str = "<tr id='vuoto'>\
                <td colspan='2' class='text-black-50'>\
                Nessun elemento presente nel panino\
                </td>\
               </tr>";
    $("#tabella > tbody").append(str);
  }
}

function paninoOk() {
  var form = $("#cart");
  if (!panino.isOk()) {
    var p = document.createElement("input");
    form.append(p);
    p.name = "error";
    p.type = "hidden";
    p.value = "1";
 }
  form.submit();
}

function notExists(elem) {
  return typeof elem === "undefined";
}
