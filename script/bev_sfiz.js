$(document).ready(function(){
    $('.change').hide();


    $('form input[type="checkbox"]').click(function() {
        var elem = this.id;
        $("." + elem).toggle(this.checked);
    });

   $('.change img').click(function() {
      var elem = this.className;
      var op;
      var s;
      var arr = elem.split(" ");

      for (var i = 0; i<arr.length; i++) {
        if (arr[i] == 'r') {
          op = 'r';
        }
        if (arr[i] == 'l') {
            op = 'l';
        }

        if (arr[i].length > 4 && arr[i].substring(0,4) == "size") {
          s = arr[i].substring(4,arr[i].length);
        }
      }
      var p = $("#p" + s);
      var val = p.val();
     console.log(p);
     console.log(val);
     if (op == 'r') {
       if (val < 10) {
         p.val(++val);
       }
     } else {
       if (val > 0) {
         p.val(--val);
       }
    }
  });
});
