function formhash(formId) {
  if ((document.getElementById("intestatario").value && document.getElementById("numerocarta").value) || (!document.getElementById("intestatario").value && !document.getElementById("numerocarta").value)) {
    if (document.getElementById("telefono").value) {
      if (document.getElementById("switchery").checked) {
        var form = document.getElementById(formId);
        var password = document.getElementById("pwd");
        var passwordbis = document.getElementById("pwdbis");
        // Crea un elemento di input che verrà usato come campo di output per la password criptata.
        if (password.value && passwordbis.value===password.value) {
          var p = document.createElement("input");
          // Aggiungi un nuovo elemento al tuo form.
          form.appendChild(p);
          p.name = "p";
          p.type = "hidden";
          p.value = hex_sha512(password.value);
          // Assicurati che la password non venga inviata in chiaro.
          password.value = "";
          passwordbis.value = "";
          // Come ultimo passaggio, esegui il 'submit' del form.
          form.submit();
        } else if (!(passwordbis.value===password.value)){
          alert("La password non è stata riscritta allo stesso modo.");
          return false;
        } else {
          alert("Campi vuoti nella sezione delle password");
          return false;
        }
      } else {
        // Come ultimo passaggio, esegui il 'submit' del form.
        form.submit();
      }
    } else {
      alert("Inserire il numero di telefono.");
      return false;
    }
  } else {
    alert("Inserire sia il numero della carta che l'intestatario.");
    return false;
  }
}
