$(document).ready(function(){

  $(".dropdown-menu button").click(function() {
    showElement($(this).attr("id"), $(this).text());
  });

  $('#errorNotification').bind('DOMNodeInserted', function() {
    deleteBox("#error");
  });

  if($('#errorNotification').length != 0) {
    deleteBox("#error");
    deleteBox("#success");
  }
});

function deleteBox(id) {
  setTimeout(function() {
    $(id).remove();
  }, 3000);
}

function showElement(id, text) {
  $("#menuIngredienti").text(text);
  if (window.XMLHttpRequest) {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
  } else {
      // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          $("#dati").html(this.responseText);
      }
  };
  xmlhttp.open("GET","ingrediente.php?q="+id,true);
  xmlhttp.send();
}

function errorOccured(msg) {
  var stampa = "<div class='mx-auto col-sm-3 col-11 alert alert-danger alert-dismissible fade show' role='alert' id='error'>" +
                   msg +
                   "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                     "<span aria-hidden='true'>&times;</span>" +
                   "</button>" +
                "</div>";
  $("#errorNotification").append(stampa);
}
