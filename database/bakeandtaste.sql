-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2018 at 11:58 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bakeandtaste`
--

-- --------------------------------------------------------

--
-- Table structure for table `bevanda`
--

CREATE TABLE `bevanda` (
  `id` int(11) NOT NULL,
  `nome` varchar(15) NOT NULL,
  `prezzo` float NOT NULL,
  `sconto` double DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL COMMENT 'percorso immagine',
  `codPorzione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bevanda`
--

INSERT INTO `bevanda` (`id`, `nome`, `prezzo`, `sconto`, `img`, `codPorzione`) VALUES
(1, 'Coca-Cola', 2, NULL, 'coca.png', 1),
(2, 'Coca-Cola', 2.5, NULL, 'coca.png', 2),
(3, 'Coca-Cola', 3, 10, 'coca.png', 3),
(4, 'Fanta', 2, NULL, 'fanta.png', 1),
(5, 'Fanta', 2.5, 10, 'fanta.png', 2),
(6, 'Fanta', 3, NULL, 'fanta.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `carrello`
--

CREATE TABLE `carrello` (
  `utente` varchar(50) NOT NULL,
  `id` int(11) NOT NULL,
  `dataacquisto` date DEFAULT NULL,
  `indirizzo` varchar(100) DEFAULT NULL,
  `prezzototale` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carrello`
--

INSERT INTO `carrello` (`utente`, `id`, `dataacquisto`, `indirizzo`, `prezzototale`) VALUES
('andri.gianno@live.it', 0, NULL, NULL, 0),
('francesco.grandinett@gmail.com', 1, NULL, NULL, 0),
('giuseppegrandinetti@libero.it', 0, NULL, NULL, 0),
('mora19396@gmail.com', 0, NULL, NULL, 0),
('silvia72.avoni@gmail.com', 0, NULL, NULL, 0),
('toniacasolla@libero.it', 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cartacredito`
--

CREATE TABLE `cartacredito` (
  `numero` varchar(15) NOT NULL,
  `proprietario` varchar(30) DEFAULT NULL,
  `utente` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cartacredito`
--

INSERT INTO `cartacredito` (`numero`, `proprietario`, `utente`) VALUES
('34567890', 'Avoni Silvia', 'silvia72.avoni@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `consegna`
--

CREATE TABLE `consegna` (
  `indirizzo` varchar(100) NOT NULL,
  `utente` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consegna`
--

INSERT INTO `consegna` (`indirizzo`, `utente`) VALUES
('via zolino 4e', 'silvia72.avoni@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `inclusionebevanda`
--

CREATE TABLE `inclusionebevanda` (
  `utente` varchar(50) NOT NULL,
  `codcarrello` int(11) NOT NULL,
  `codbevanda` int(11) NOT NULL,
  `quantita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inclusioneingredienti`
--

CREATE TABLE `inclusioneingredienti` (
  `codProdPers` int(11) NOT NULL,
  `codIngr` int(11) NOT NULL,
  `tipoIngr` int(11) NOT NULL,
  `quantita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inclusioneingredienti`
--

INSERT INTO `inclusioneingredienti` (`codProdPers`, `codIngr`, `tipoIngr`, `quantita`) VALUES
(1, 1, 1, 1),
(1, 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inclusionemenu`
--

CREATE TABLE `inclusionemenu` (
  `utente` varchar(50) NOT NULL,
  `codcarrello` int(11) NOT NULL,
  `codmenu` int(11) NOT NULL,
  `quantita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inclusioneprodstand`
--

CREATE TABLE `inclusioneprodstand` (
  `codProdStand` int(11) NOT NULL,
  `codCarrello` int(11) NOT NULL,
  `utente` varchar(50) NOT NULL,
  `quantita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inclusionesnack`
--

CREATE TABLE `inclusionesnack` (
  `utente` varchar(50) NOT NULL,
  `codcarrello` int(11) NOT NULL,
  `codsnack` int(11) NOT NULL,
  `quantita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingrediente`
--

CREATE TABLE `ingrediente` (
  `id` int(11) NOT NULL,
  `codTipo` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `prezzo` float NOT NULL,
  `sconto` int(11) DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL COMMENT 'percorso immagine'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingrediente`
--

INSERT INTO `ingrediente` (`id`, `codTipo`, `nome`, `prezzo`, `sconto`, `img`) VALUES
(1, 1, 'Pane', 0.2, NULL, 'pane.png'),
(1, 2, 'Carne di manzo', 3.5, NULL, 'ham.png'),
(1, 4, 'Insalata', 0.2, NULL, 'insalata.png'),
(2, 1, 'Pane di semola', 0.4, NULL, 'pane.png'),
(2, 2, 'Carne di bufalo', 4, NULL, 'ham.png'),
(3, 2, 'Cotoletta', 2.5, NULL, 'cotoletta.png'),
(4, 2, 'Prosciutto Cotto', 2, NULL, 'proscitto1.png');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `prezzo` float NOT NULL,
  `sconto` int(11) DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nome`, `prezzo`, `sconto`, `img`) VALUES
(1, 'Chicken Menu', 7, NULL, 'chickenMenu.jpg'),
(2, 'My Menu', 8.2, NULL, 'myMenu.jpg'),
(3, 'Vegan Menu', 10.2, NULL, 'veganMenu.jpg'),
(12, 'Menu Gustoso', 7.5, 50, 'menu_aggressivo.jpg'),
(13, 'Double Menu', 8.2, NULL, 'double_menu.jpg'),
(14, 'Menu Sandwich', 6.4, NULL, 'menu_sandwich.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `notifica`
--

CREATE TABLE `notifica` (
  `id` int(11) NOT NULL,
  `destinatario` varchar(50) NOT NULL,
  `messaggio` varchar(255) NOT NULL,
  `visto` tinyint(1) NOT NULL,
  `mittente` varchar(50) NOT NULL,
  `codCarrello` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `porzione`
--

CREATE TABLE `porzione` (
  `id` int(11) NOT NULL,
  `nome` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `porzione`
--

INSERT INTO `porzione` (`id`, `nome`) VALUES
(1, 'Piccola'),
(2, 'Media'),
(3, 'Grande');

-- --------------------------------------------------------

--
-- Table structure for table `presenzabevanda`
--

CREATE TABLE `presenzabevanda` (
  `codmenu` int(11) NOT NULL,
  `codbevanda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presenzabevanda`
--

INSERT INTO `presenzabevanda` (`codmenu`, `codbevanda`) VALUES
(1, 2),
(2, 3),
(3, 4),
(12, 5),
(13, 3),
(14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `presenzaprodstand`
--

CREATE TABLE `presenzaprodstand` (
  `codmenu` int(11) NOT NULL,
  `codprodstand` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presenzaprodstand`
--

INSERT INTO `presenzaprodstand` (`codmenu`, `codprodstand`) VALUES
(1, 3),
(2, 4),
(3, 2),
(12, 4),
(13, 1),
(14, 9);

-- --------------------------------------------------------

--
-- Table structure for table `presenzasnack`
--

CREATE TABLE `presenzasnack` (
  `codmenu` int(11) NOT NULL,
  `codsnack` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `presenzasnack`
--

INSERT INTO `presenzasnack` (`codmenu`, `codsnack`) VALUES
(1, 4),
(2, 3),
(3, 2),
(12, 2),
(13, 5),
(14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `prodottopersonalizzato`
--

CREATE TABLE `prodottopersonalizzato` (
  `id` int(11) NOT NULL,
  `prezzofinale` float NOT NULL,
  `codtipo` int(11) NOT NULL,
  `utente` varchar(50) NOT NULL,
  `codcarrello` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodottopersonalizzato`
--

INSERT INTO `prodottopersonalizzato` (`id`, `prezzofinale`, `codtipo`, `utente`, `codcarrello`) VALUES
(1, 4.2, 1, 'francesco.grandinett@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prodottostandard`
--

CREATE TABLE `prodottostandard` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descrizione` varchar(255) DEFAULT NULL,
  `prezzo` float NOT NULL,
  `sconto` int(11) DEFAULT NULL,
  `codTipo` int(11) NOT NULL,
  `img` varchar(30) DEFAULT NULL COMMENT 'percorso immagine'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodottostandard`
--

INSERT INTO `prodottostandard` (`id`, `nome`, `descrizione`, `prezzo`, `sconto`, `codTipo`, `img`) VALUES
(1, 'Double Cheese Burger', 'Ingredienti: formaggio, carne bovina, cipolla', 5, NULL, 1, 'panino.jpg'),
(2, 'Veggy Burger', 'Ingredienti: formaggio, carne di soia, cipolla, carote, aglio', 9, NULL, 1, 'veggyBurger.jpg'),
(3, 'Chicken Burger', 'Ingredienti: formaggio, cotoletta di pollo, insalata, pomodori', 5.5, NULL, 1, 'chickenBurger.jpg'),
(4, 'Buffalo Burger', 'Ingredienti: carne di bufalo, bacon, insalata, pomodori, cipolla', 8, NULL, 1, 'buffaloBurger.jpg'),
(5, 'Fish Burger', 'Ingredienti: filetto di merluzzo impanato, cipolle lattuga, salsa allo yogurt', 7, NULL, 1, 'fishBurger.jpg'),
(8, 'Panino Provolone', 'Ingredienti: prosciutto cotto e fontina', 5.6, NULL, 1, 'panino-cotto-e-fontina.jpg'),
(9, 'Sandwich Contadino', 'Ingredienti: prosciutto cotto, formaggio, pomodori', 2, 30, 1, 'Facciamoci_un_panino.jpg'),
(10, 'Panino con le polpette', 'Ingredienti: panino con le polpette, che altro?', 3.5, NULL, 1, 'panini-polpette02.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `snack`
--

CREATE TABLE `snack` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `prezzo` float NOT NULL,
  `sconto` double DEFAULT NULL,
  `img` varchar(30) DEFAULT NULL COMMENT 'percorso immagine',
  `codPorzione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `snack`
--

INSERT INTO `snack` (`id`, `nome`, `prezzo`, `sconto`, `img`, `codPorzione`) VALUES
(1, 'Patatine Fritte', 3, NULL, 'patate.png', 1),
(2, 'Patatine Fritte', 3.52, NULL, 'patate.png', 2),
(3, 'Patatine Fritte', 3.99, NULL, 'patate.png', 3),
(4, 'Crocchette di pollo', 3.2, 35, 'nuggets.png', 1),
(5, 'Crocchette di pollo', 3.6, NULL, 'nuggets.png', 2),
(6, 'Crocchette di pollo', 4, NULL, 'nuggets.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tentativologin`
--

CREATE TABLE `tentativologin` (
  `user_id` varchar(50) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tentativologin`
--

INSERT INTO `tentativologin` (`user_id`, `time`) VALUES
('mora19396@gmail.com', '1530350965'),
('silvia72.avoni@gmail.com', '1530350947'),
('silvia72.avoni@gmail.com', '1530350955'),
('silvia72.avoni@gmail.com', '1530350979');

-- --------------------------------------------------------

--
-- Table structure for table `tipoingrediente`
--

CREATE TABLE `tipoingrediente` (
  `id` int(11) NOT NULL,
  `nome` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipoingrediente`
--

INSERT INTO `tipoingrediente` (`id`, `nome`) VALUES
(1, 'Pane'),
(2, 'Carne'),
(3, 'Formaggio'),
(4, 'Verdura'),
(5, 'Altro');

-- --------------------------------------------------------

--
-- Table structure for table `tipoprodotto`
--

CREATE TABLE `tipoprodotto` (
  `id` int(11) NOT NULL,
  `nome` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipoprodotto`
--

INSERT INTO `tipoprodotto` (`id`, `nome`) VALUES
(1, 'Panino'),
(2, 'Insalata');

-- --------------------------------------------------------

--
-- Table structure for table `utente`
--

CREATE TABLE `utente` (
  `user` varchar(50) NOT NULL,
  `pwd` char(128) NOT NULL,
  `fornitore` tinyint(1) NOT NULL,
  `nome` varchar(15) NOT NULL,
  `cognome` varchar(20) NOT NULL,
  `sesso` char(1) NOT NULL COMMENT 'M/F/A',
  `dataNascita` date NOT NULL,
  `numeroTelefono` varchar(15) NOT NULL,
  `fotoProfilo` varchar(30) DEFAULT NULL COMMENT 'percorso immagine',
  `codice` char(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utente`
--

INSERT INTO `utente` (`user`, `pwd`, `fornitore`, `nome`, `cognome`, `sesso`, `dataNascita`, `numeroTelefono`, `fotoProfilo`, `codice`) VALUES
('adminSupremo', '99c2d11cd4955fc79289181ffbf8e3b5044404a96ef349aad8463a39c84a15ac27a595b40f3a80fc58e9e94a575b2934a249d393f60dbd7d53d6038d6339a22c', 1, 'Admin', 'Supremo', 'M', '1960-08-09', '3474864891', NULL, '733e9b1f876e8161a8c058023aee02c2a7510688b393f1a4c3b82729a6cc12ecfcd1842e8c7c3118967d9f9fe94016995945b8deff06ad93a3dc1d8c9e67e81b'),
('andri.gianno@live.it', '92c639c14d022c2bd66a07ff45a23982c4a8f686c7776fce2a76e782150ebf3a6442f01bfada8accb645d09d87106f22b3988273252ab3c33179d13478f54ecc', 0, 'Andrea', 'Giannini', 'M', '1996-10-17', '3207481078', NULL, 'e72410ecefad00cee254048b999d51155163513525ea197de30f7df0af2d2cb7055dab09ef43926699875cc80330abc5a945bb7c37ce0f57894de26623d38c2d'),
('francesco.grandinett@gmail.com', 'd18f2a37affa93b039e09bfe2d15759aacfb3e94fa94da4337def9f6a8bd1425a009af866804bba1084c9274de2b83f656e792f04e61057256dc90bfde790d3b', 0, 'Francesco', 'Grandinetti', 'M', '1996-04-27', '3474864891', NULL, '200b31316bb24c67f167e6a1b5f38d826169d9b6a0e5f0c7ad2e27818ffe13fcbdfcf31dd04dc245cc775429763daf58095e99424c7d28840295ba086f647aa7'),
('giuseppegrandinetti@libero.it', '7c6daed050b706df9c0854889868fe4f888bf3b87092527268705a10bb151f1335cdc6d31842d62a9b968629db0a0bdb2bdd4ea7dc5517f6b44dc52eac1ac887', 0, 'Giuseppe', 'Grandinetti', 'M', '1953-02-28', '3451126296', NULL, '6a3f2fa7241613495f18396e1971cd2fa4b40684fed2499e454a9e6c5457e28f3c1b38806d2e949767d13a285d2fbea48c7da0d118ca90ad5327df74f49f422e'),
('mora19396@gmail.com', 'de7910d5618fa7fd4699abbf8f0ef0536193a97f8fc295a39365599d0f035d4cf36213512fc5d71fd8e545166c3307d4eb493d993986d767fee1be7bb86de598', 0, 'Elena', 'Morelli', 'F', '1996-03-19', '3492447002', NULL, 'eeefd4191ac8196884c49fb1c65aa30394d7b6ac712227442a280402827be642096584c5575818ff77c3e522b1a890bf32dc70f79f94fca66c630e1ab59ae6c8'),
('silvia72.avoni@gmail.com', '7f2c0b83e19b9e7370004e0d4702327087fe33566b26f1f6dbeefce6f695daf54c6f2b18e1bee5eecba4a6f07eb86756267fe8b5e5e1ffcc4a56547873b63370', 0, 'Silvia', 'Avoni', 'F', '1972-04-16', '3480032639', NULL, '96d1b358cd81887b616b78d9e7ef2d59130bcd2dc346e2fdf1c7fb276bc489d0f4d0d128a810cf39510cff09cde475d4525d053e9119ae05397c8fb73514e814'),
('toniacasolla@libero.it', '7ddd504b08e6061b2e1f8c364c25305cc7d514eaeb16d127cd49d6b4ccb16c31410a632fd32982057356dea8e13d70026af909182fc52b45531f60eb7b263bbd', 0, 'Antonia', 'Casolla', 'F', '1957-10-30', '3392945359', NULL, '1771d9b29b21fbdc52f55e87bf5341f6e04d03e1b78cba17abca681a41076ce6021ddee9e54da80ca0a813627ef0c3782ac3a2a63d4bdfffbaa4cce848383c2b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bevanda`
--
ALTER TABLE `bevanda`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codPorzione` (`codPorzione`);

--
-- Indexes for table `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`utente`,`id`);

--
-- Indexes for table `cartacredito`
--
ALTER TABLE `cartacredito`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `utente` (`utente`);

--
-- Indexes for table `consegna`
--
ALTER TABLE `consegna`
  ADD PRIMARY KEY (`indirizzo`,`utente`),
  ADD KEY `utente` (`utente`);

--
-- Indexes for table `inclusionebevanda`
--
ALTER TABLE `inclusionebevanda`
  ADD PRIMARY KEY (`utente`,`codcarrello`,`codbevanda`),
  ADD KEY `codbevanda` (`codbevanda`);

--
-- Indexes for table `inclusioneingredienti`
--
ALTER TABLE `inclusioneingredienti`
  ADD PRIMARY KEY (`codProdPers`,`codIngr`,`tipoIngr`),
  ADD KEY `codIngr` (`codIngr`,`tipoIngr`),
  ADD KEY `tipoIngr` (`tipoIngr`,`codIngr`);

--
-- Indexes for table `inclusionemenu`
--
ALTER TABLE `inclusionemenu`
  ADD PRIMARY KEY (`utente`,`codcarrello`,`codmenu`),
  ADD KEY `codmenu` (`codmenu`);

--
-- Indexes for table `inclusioneprodstand`
--
ALTER TABLE `inclusioneprodstand`
  ADD PRIMARY KEY (`codProdStand`,`codCarrello`,`utente`),
  ADD KEY `utente` (`utente`,`codCarrello`);

--
-- Indexes for table `inclusionesnack`
--
ALTER TABLE `inclusionesnack`
  ADD PRIMARY KEY (`utente`,`codcarrello`,`codsnack`),
  ADD KEY `codsnack` (`codsnack`);

--
-- Indexes for table `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD PRIMARY KEY (`id`,`codTipo`),
  ADD KEY `tipo` (`codTipo`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destinatario` (`destinatario`),
  ADD KEY `mittente` (`mittente`,`codCarrello`);

--
-- Indexes for table `porzione`
--
ALTER TABLE `porzione`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presenzabevanda`
--
ALTER TABLE `presenzabevanda`
  ADD PRIMARY KEY (`codmenu`,`codbevanda`),
  ADD KEY `codbevanda` (`codbevanda`);

--
-- Indexes for table `presenzaprodstand`
--
ALTER TABLE `presenzaprodstand`
  ADD PRIMARY KEY (`codmenu`,`codprodstand`),
  ADD KEY `codprodstand` (`codprodstand`);

--
-- Indexes for table `presenzasnack`
--
ALTER TABLE `presenzasnack`
  ADD PRIMARY KEY (`codmenu`,`codsnack`),
  ADD KEY `codsnack` (`codsnack`);

--
-- Indexes for table `prodottopersonalizzato`
--
ALTER TABLE `prodottopersonalizzato`
  ADD PRIMARY KEY (`id`),
  ADD KEY `PK_TIPO` (`codtipo`),
  ADD KEY `PK_CARR` (`utente`,`codcarrello`);

--
-- Indexes for table `prodottostandard`
--
ALTER TABLE `prodottostandard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codTipo` (`codTipo`);

--
-- Indexes for table `snack`
--
ALTER TABLE `snack`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codPorzione` (`codPorzione`);

--
-- Indexes for table `tentativologin`
--
ALTER TABLE `tentativologin`
  ADD PRIMARY KEY (`user_id`,`time`);

--
-- Indexes for table `tipoingrediente`
--
ALTER TABLE `tipoingrediente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipoprodotto`
--
ALTER TABLE `tipoprodotto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bevanda`
--
ALTER TABLE `bevanda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ingrediente`
--
ALTER TABLE `ingrediente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `notifica`
--
ALTER TABLE `notifica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `porzione`
--
ALTER TABLE `porzione`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prodottopersonalizzato`
--
ALTER TABLE `prodottopersonalizzato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prodottostandard`
--
ALTER TABLE `prodottostandard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `snack`
--
ALTER TABLE `snack`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tipoingrediente`
--
ALTER TABLE `tipoingrediente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tipoprodotto`
--
ALTER TABLE `tipoprodotto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bevanda`
--
ALTER TABLE `bevanda`
  ADD CONSTRAINT `bevanda_ibfk_1` FOREIGN KEY (`codPorzione`) REFERENCES `porzione` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `carrello`
--
ALTER TABLE `carrello`
  ADD CONSTRAINT `PK_UTENTE` FOREIGN KEY (`utente`) REFERENCES `utente` (`user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cartacredito`
--
ALTER TABLE `cartacredito`
  ADD CONSTRAINT `cartacredito_ibfk_1` FOREIGN KEY (`utente`) REFERENCES `utente` (`user`);

--
-- Constraints for table `consegna`
--
ALTER TABLE `consegna`
  ADD CONSTRAINT `consegna_ibfk_1` FOREIGN KEY (`utente`) REFERENCES `utente` (`user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inclusionebevanda`
--
ALTER TABLE `inclusionebevanda`
  ADD CONSTRAINT `inclusionebevanda_ibfk_1` FOREIGN KEY (`codbevanda`) REFERENCES `bevanda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inclusionebevanda_ibfk_2` FOREIGN KEY (`utente`,`codcarrello`) REFERENCES `carrello` (`utente`, `id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inclusioneingredienti`
--
ALTER TABLE `inclusioneingredienti`
  ADD CONSTRAINT `inclusioneingredienti_ibfk_2` FOREIGN KEY (`codProdPers`) REFERENCES `prodottopersonalizzato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inclusioneingredienti_ibfk_3` FOREIGN KEY (`tipoIngr`,`codIngr`) REFERENCES `ingrediente` (`codTipo`, `id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inclusionemenu`
--
ALTER TABLE `inclusionemenu`
  ADD CONSTRAINT `inclusionemenu_ibfk_1` FOREIGN KEY (`codmenu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inclusionemenu_ibfk_2` FOREIGN KEY (`utente`,`codcarrello`) REFERENCES `carrello` (`utente`, `id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inclusioneprodstand`
--
ALTER TABLE `inclusioneprodstand`
  ADD CONSTRAINT `inclusioneprodstand_ibfk_1` FOREIGN KEY (`codProdStand`) REFERENCES `prodottostandard` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inclusioneprodstand_ibfk_2` FOREIGN KEY (`utente`,`codCarrello`) REFERENCES `carrello` (`utente`, `id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inclusionesnack`
--
ALTER TABLE `inclusionesnack`
  ADD CONSTRAINT `inclusionesnack_ibfk_1` FOREIGN KEY (`codsnack`) REFERENCES `snack` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inclusionesnack_ibfk_2` FOREIGN KEY (`utente`,`codcarrello`) REFERENCES `carrello` (`utente`, `id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD CONSTRAINT `ingrediente_ibfk_1` FOREIGN KEY (`codTipo`) REFERENCES `tipoingrediente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifica`
--
ALTER TABLE `notifica`
  ADD CONSTRAINT `notifica_ibfk_1` FOREIGN KEY (`destinatario`) REFERENCES `utente` (`user`),
  ADD CONSTRAINT `notifica_ibfk_2` FOREIGN KEY (`mittente`,`codCarrello`) REFERENCES `carrello` (`utente`, `id`);

--
-- Constraints for table `presenzabevanda`
--
ALTER TABLE `presenzabevanda`
  ADD CONSTRAINT `presenzabevanda_ibfk_1` FOREIGN KEY (`codbevanda`) REFERENCES `bevanda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `presenzabevanda_ibfk_2` FOREIGN KEY (`codmenu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `presenzaprodstand`
--
ALTER TABLE `presenzaprodstand`
  ADD CONSTRAINT `presenzaprodstand_ibfk_1` FOREIGN KEY (`codmenu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `presenzaprodstand_ibfk_2` FOREIGN KEY (`codprodstand`) REFERENCES `prodottostandard` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `presenzasnack`
--
ALTER TABLE `presenzasnack`
  ADD CONSTRAINT `presenzasnack_ibfk_1` FOREIGN KEY (`codmenu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `presenzasnack_ibfk_2` FOREIGN KEY (`codsnack`) REFERENCES `snack` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `prodottopersonalizzato`
--
ALTER TABLE `prodottopersonalizzato`
  ADD CONSTRAINT `PK_CARR` FOREIGN KEY (`utente`,`codcarrello`) REFERENCES `carrello` (`utente`, `id`),
  ADD CONSTRAINT `PK_TIPO` FOREIGN KEY (`codtipo`) REFERENCES `tipoprodotto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `prodottostandard`
--
ALTER TABLE `prodottostandard`
  ADD CONSTRAINT `prodottostandard_ibfk_1` FOREIGN KEY (`codTipo`) REFERENCES `tipoprodotto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `snack`
--
ALTER TABLE `snack`
  ADD CONSTRAINT `snack_ibfk_1` FOREIGN KEY (`codPorzione`) REFERENCES `porzione` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tentativologin`
--
ALTER TABLE `tentativologin`
  ADD CONSTRAINT `tentativologin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `utente` (`user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
