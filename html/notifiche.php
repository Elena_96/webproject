<div class="collapse navbar-collapse" id="notifiche">
  <section>
    <img src="../img/bell.png" alt="Notifiche" width="200">
    <h1>Notifiche</h1>
  </section>
  <section>
    <ul>
      <?php foreach ($Notifiche as $value) { ?>
      <li class="row" id="<?php echo $value->id; ?>">
        <p class="col-10"><?php echo $value->msg; ?></p>
        <i class="material-icons col-2" id="<?php echo $value->id; ?>">cancel</i>
      </li>
    <?php } ?>
    </ul>
  </section>
</div>
