<?php
function inserisciOfferta($mysqli,$myPanino) {
  $email = $_SESSION["email"];
  $cartId = getCartId($mysqli, $email);
  $format = "col-sm-10 col-10";
  if ($cartId < 0) {
    echo "errore";
  }
    $prezzo = 0;
    $id = $_POST["idOfferta"];
    $type = after("-",$id);
    $newId = before("-",$id);
    $quant = $_POST["p".$id];
    $quantitaGiaPresente = 0;
    if ($quant==0){
      return "Attenzione! La quantità non può essere uguale a zero.";
    }else{
    switch($type){
      case "drink": $query1 = "SELECT quantita
                               FROM inclusioneBevanda
                               WHERE codbevanda = ".$newId .
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    $result = $mysqli->query($query1);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                          $quantitaGiaPresente += $row["quantita"];
                      }
                    }
                    if($quantitaGiaPresente > 0){
                      $tot = $quantitaGiaPresente + $quant;
                      $stmt = "UPDATE inclusionebevanda
                               SET quantita =".$tot.
                               " WHERE codbevanda =".$newId .
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    }else {
                      $stmt = "INSERT INTO inclusionebevanda(utente, codcarrello,codbevanda, quantita)
                               VALUES ('$email', '$cartId','$newId','$quant')";
                    }
                    $query = "SELECT prezzo FROM bevanda WHERE id = ".$newId; break;

      case "menu":  $query1 = "SELECT quantita
                               FROM inclusioneMenu
                               WHERE codmenu = ".$newId.
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    $result = $mysqli->query($query1);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                          $quantitaGiaPresente += $row["quantita"];
                      }
                    }
                    if($quantitaGiaPresente > 0){
                      $tot = $quantitaGiaPresente + $quant;
                      $stmt = "UPDATE inclusioneMenu
                               SET quantita =".$tot.
                               " WHERE codmenu =".$newId.
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    }else {
                      $stmt = "INSERT INTO inclusionemenu(utente, codcarrello,codmenu, quantita)
                                 VALUES ('$email','$cartId','$newId','$quant')";
                    }
                    $query = "SELECT prezzo FROM menu WHERE id = ".$newId; break;

     case "panino": $query1 = "SELECT quantita
                               FROM inclusioneProdStand
                               WHERE codProdStand = ".$newId.
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    $result = $mysqli->query($query1);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                          $quantitaGiaPresente += $row["quantita"];
                      }
                    }
                    if($quantitaGiaPresente > 0){
                      $tot = $quantitaGiaPresente + $quant;
                      $stmt = "UPDATE inclusioneProdStand
                               SET quantita =".$tot.
                               " WHERE codProdStand =".$newId.
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    }else {
                      $stmt = "INSERT INTO inclusioneprodstand(codProdStand, codcarrello,utente, quantita)
                              VALUES ('$newId', '$cartId', '$email','$quant')";
                    }
                    $query = "SELECT prezzo FROM prodottostandard WHERE id = ".$newId; break;

      case "snack": $query1 = "SELECT quantita
                               FROM inclusionesnack
                               WHERE codsnack = ".$newId.
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    $result = $mysqli->query($query1);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                          $quantitaGiaPresente += $row["quantita"];
                      }
                    }
                    if($quantitaGiaPresente > 0){
                      $tot = $quantitaGiaPresente + $quant;
                      $stmt = "UPDATE inclusionesnack
                               SET quantita =".$tot.
                               " WHERE codsnack =".$newId.
                               " AND codCarrello = " . $cartId .
                               " AND utente = '" . $email . "'";
                    }else {
                      $stmt = "INSERT INTO inclusionesnack(utente, codcarrello,codsnack, quantita)
                               VALUES ('$email','$cartId','$newId','$quant')";
                    }
                    $query = "SELECT prezzo FROM snack WHERE id = ".$newId; break;
    }

    $result = $mysqli->query($query);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $prezzo += $row["prezzo"]*$quant;
      }
    }
    if ($mysqli->query($stmt) == TRUE){
      updateCartTotal($mysqli, $cartId, $email, $prezzo);
      return "";
    } else {
      return $mysqli->error;
    }
  }
}

include "utils/functions.php";
include "utils/db_connect.php";
sec_session_start();
$_SESSION["lastPage"] = 'offerte.php';
$myArray = array();

class Food {
  public $id;
  public $quantita;
  public $prezzo;
  public $name;
  public $porzione;
  public $img;
  public $sconto;

  function __construct($id,$name,$prezzo,$sconto,$img,$porzione,$tipologia) {
    $this->id = $id."-".$tipologia;
    $this->name = $name;
    $this->quantita = 0;
    $this->porzione = $porzione;
    $this->prezzo= $prezzo;
    $this->sconto =$sconto;
    $this->img = $img;
  }
}

$conn = connectToDatabase();

$inserito = false;
$msg = "";
if(!empty($_POST['idOfferta'])) {
  if (!login_check($conn)) {
    header('Location: login.php?error=99');
  }
  $msg = inserisciOfferta($conn,$_POST['idOfferta']);
  $inserito = true;
}

$query_sql="SELECT m.id as id, m.nome as nome, m.prezzo as prezzo, m.sconto as sconto, m.img as img
             FROM menu m
             WHERE m.sconto IS NOT NULL";

$result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $myMenu = new Food($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["img"],"","menu");
       $myArray[]=$myMenu;
  }
}

$query_sql="SELECT p.id as id, p.nome as nome, p.prezzo as prezzo, p.sconto as sconto, p.img as img
            FROM prodottostandard p
            WHERE p.sconto IS NOT NULL";

$result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $myFood = new Food($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["img"],"","panino");
       $myArray[]=$myFood;
     }
   }

   $query_sql="SELECT s.id as id, s.nome as nome, s.prezzo as prezzo, s.sconto as sconto, s.img as img , p.nome as porz
               FROM snack s, porzione p
               WHERE s.codPorzione = p.id
               AND s.sconto IS NOT NULL";

   $result = $conn->query($query_sql);
     if ($result->num_rows > 0) {
       while($row = $result->fetch_assoc()) {
       $mySnack = new Food($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["img"],$row["porz"],"snack");
       $myArray[]=$mySnack;
         }
       }

$query_sql="SELECT b.id as id, b.nome as nome, b.prezzo as prezzo, b.sconto as sconto, b.img as img , p.nome as porz
            FROM bevanda b, porzione p
            WHERE b.codPorzione = p.id
            AND b.sconto IS NOT NULL";

$result = $conn->query($query_sql);

 if ($result->num_rows > 0) {
   while($row = $result->fetch_assoc()) {
      $myDrink = new Food($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["img"],$row["porz"],"drink");
      $myArray[]=$myDrink;
    }
  }

?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <title>Offerte</title>
    <?php require 'utils/commons.html'; ?>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
    <script src="../script/prodotti.js"></script>
    <link rel="stylesheet" href="../css/offerte.css">
  </head>
  <body>
    <?php require 'navbar.php' ?>
    <section class="container-fluid page" id="content">
      <div>
        <?php
          $format = "col-sm-10 col-10";
          if($inserito && empty($msg)) {
        ?>
        <div class="row msg">
          <?php echo successOccured($format,"Inserimento completato! Il tuo articolo è stato aggiunto al carrello.");?>
        </div>
        <?php
      } else if($inserito){
        ?>
        <div class="row msg">
          <?php echo errorOccured($format,$msg);?>
        </div>
        <?php
          }
        ?>
      </div>
      <header>
        <h1>Le nostre offerte</h1>
        <h2>Magnifici sconti da non poter perdere</h2>
      </header>
      <section>
        <?php if(count($myArray) > 0) { foreach ($myArray as $key => $value) {?>
          <form action="offerte.php" method="post" class="row">
            <fieldset class="border rounded col-11 mx-auto">
                <div class="container-fluid">
                  <div class="row align-items-center">
                  <div class="immagine col-12 col-md-5">
                      <img src= <?php echo getImgFolder().$value->img?> alt="<?php echo $value->name?>" class="rounded float-left img-fluid">
                  </div>
                  <div class="col-8 offset-1 col-md-4">
                    <div class="paragraph">
                    <h3><?php echo $value->name;?></h3>
                    <p><?php
                      $tipo = after("-",$value->id);
                      $newId = before("-",$value->id);
                      if (strcmp($tipo,"drink") == 0 || strcmp($tipo,"snack") == 0) { ?>
                        <span id="<?php echo "t".$newId; ?>"> Porzione: <?php echo $value->porzione;?></span></br>
                        <script type="text/javascript">$('#t'+ <?php echo $newId; ?>).show()</script><?php
                      } else { ?>
                        <script type="text/javascript">$('#t'+ <?php echo $newId; ?>).hide()</script><?php
                      } ?>
                      Prezzo: <?php echo number_format($value->prezzo,2)?>€ </br>
                      <span class="hide"> Sconto: <?php echo $value->sconto?>%</span>
                    </p>
                    <div class="change">
                      <div class="arrow">
                        <img src="../img/arrow_l.png" alt="Diminuzione quantità" class="img-fluid l <?php echo"size".$value->id?>">
                        <input type="text" id="<?php echo "p".$value->id ?>" name = "<?php echo "p".$value->id?>" value="<?php echo $value->quantita?>" readonly/>
                        <img src="../img/arrow_r.png" alt="Aumento quantità" class="img-fluid r <?php echo"size".$value->id?>"><br/>
                        <input type="hidden" name="idOfferta" value="<?php echo $value->id?>">
                      </div>
                    </div>
                  </div>
                  </div>
                  <div class="sconto col-1 col-md-1">
                    <div class="sale">-<?php echo $value->sconto ?>%</div>
                  </div>
                  <div class="col-1 col-md-1 align-self-end carrello">
                    <input type="image" src=<?php echo '"'.getImgFolder().'addToCart.png"'; ?> alt="Icona aggiungi al carrello">
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        <?php } }
        else {
        ?>
        <div class="row">
          <div class="mx-auto rounded col-sm-3 col-10" id="no-offerte">
            <span> :( </span> <br/> <br/>
            <span>
              Siamo spiacenti, ma al momento non abbiamo alcuna offerta disponibile. <br/>
              Riprova in un altro momento e potrai essere più fortunato!!!
            </span>
          </div>
        </div>
      <?php } ?>
      </section>
    </section>
  </body>
</html>
