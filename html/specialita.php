<?php
  include 'utils/functions.php';
  include 'utils/db_connect.php';
  sec_session_start();

  //funzione per l'inserimento di un nuovo panino
  function inserisciPanino($mysqli, $panino) {
    $email = $_SESSION["email"];
    $cartId = getCartId($mysqli, $email);
    $prezzo = $panino["prezzo"];
    // se cartId < 0 vuol dire che il carrello relativo all'utente non esiste, errore!
    if ($cartId < 0) {
      return "No user's cart found";
    }
    // creo il panino sul database
    $stmt = "INSERT INTO prodottopersonalizzato (prezzofinale, codtipo, utente, codcarrello) VALUES (".$prezzo.", 1, '".$email."', ".$cartId.")";
    if ($mysqli->query($stmt) === TRUE) {
      $panino_id = $mysqli->insert_id;
      /*
       * creo una stringa che conterrà tutti i vari ingredienti che sono inclusi nel panino,
       * sarà un inserimento multiplo
       */
      $stmt = "INSERT INTO inclusioneIngredienti (codProdPers, codIngr, tipoIngr, quantita) VALUES ";
      $soglia = $panino["numIngr"];
      for ($i=0; $i < $soglia ; $i++) {
        $quant = 1;
        while($i + 1 < $soglia && $panino["ingr"][$i] == $panino["ingr"][$i+1]) {
          $i++;
          $quant++;
        }
        // la stringa ingr è del tipo aa_bb dove aa = codiceIngrediente e bb = tipoIngrediente
        list($codIng, $tipoIng) = explode("_", $panino["ingr"][$i]);
        $stmt .= "(".$panino_id.", ".$codIng.", ".$tipoIng.", ".$quant.")";
        if ($i < $soglia - 1) {
          $stmt .= ", ";
        }
      }
      if ($mysqli->query($stmt) === TRUE){
        updateCartTotal($mysqli, $cartId, $email, $prezzo);
        return "";
      } else {
        return $mysqli->error;
      }
    } else {
      return $mysqli->error;
    }
  }

  $_SESSION["lastPage"] = 'specialita.php';
  $inserito = false;
  $msg = "";
  if(isset($_POST['error'])) {
    $inserito = true;
    $msg = "Non è possibile comprare un panino senza pane e/o carne!";
  } else if (isset($_POST['count'])) {
       $mysqli = connectToDatabase();
       $arr = array();
       for($x = 0; $x < $_POST["count"]; $x++)  {
         $arr[$x] = $_POST["ing".($x+1)];
       }
       $panino = array('prezzo' => $_POST["totale"], 'numIngr' => $_POST["count"], 'ingr' => $arr);
       if (!login_check($mysqli)) {
         $_SESSION["paninoSalvato"] = $panino;
         header('Location: login.php?error=99');
       } else {
         $msg = inserisciPanino($mysqli, $panino);
         $inserito = true;
        }
  } else {
      $mysqli = connectToDatabase();
      if (login_check($mysqli) && isset($_SESSION['paninoSalvato']) && $_SESSION['paninoSalvato'] != null) {
           $msg = inserisciPanino($mysqli, $_SESSION['paninoSalvato']);
           $inserito = true;
           $_SESSION['paninoSalvato'] = null;
       }
  }
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <?php
      require 'utils/commons.html';
     ?>
    <title>Bake and Taste</title>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/navbar.css">
    <link rel="stylesheet" href="../css/specialita.css">
    <script src="../script/specialita.js"></script>
    <script src="../script/crea.js"></script>
  </head>
  <body>
    <?php require 'navbar.php' ?>
    <section class="container-fluid page" id="content">
      <main class="row">
        <div class="container-fluid">
          <!-- Titolo -->
          <div class="row">
            <h1 class="col mx-auto">Crea la tua specialità</h1>
          </div>
          <!-- Immagine + tabella -->
          <div id="panino-scelto" class="row">
            <div class="col-6 col-sm-7" id="costruisci">
            </div>
            <div class="col container-fluid">
                <div class="row">
                  <table id="tabella" class="col">
                    <thead>
                      <tr>
                        <th id="comp" scope="col">Componente</th>
                        <th id="prezzo" scope="col">Prezzo</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr id="vuoto">
                        <td colspan="2" class="text-black-50">
                          Nessun elemento presente nel panino
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th id="tot" scope="row">TOTALE:</th>
                        <td headers="tot prezzo">0€</td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="row">
                  <form id="cart" class="mx-auto" action="specialita.php" method="post">
                    <input type="image" src=<?php echo '"'.getImgFolder().'addToCart.png"'; ?> name="cart" alt="Aggiungi al carrello" class="img-fluid" onclick="paninoOk()">
                    <input type="hidden" name="count" value="0">
                    <input type="hidden" name="totale" value="0">
                  </form>
                </div>
            </div>
          </div>
          <div id="errorNotification">
            <?php
              $format = "col-sm-3 col-11";
              if($inserito && empty($msg)) {
                echo successOccured($format,"Inserimento completato! Il tuo articolo è stato aggiunto al carrello.");
              } else if($inserito){
                echo errorOccured($format,$msg);
              }
             ?>
          </div>
          <!-- Bottone scelta menu -->
          <div class="row">
              <div class="dropdown mx-auto">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="menuIngredienti" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Ingredienti
                </button>
                <div class="dropdown-menu" aria-labelledby="menuIngredienti">
                  <?php
                        $mysqli = connectToDatabase();
                        $sql = "SELECT * FROM tipoIngrediente";
                        $res = $mysqli->query($sql);
              					if ($res->num_rows > 0) {
              						while($row = $res->fetch_assoc()) {
              		 ?>
                            <button id="<?php echo $row["id"]; ?>" class="dropdown-item" type="button"><?php echo ucfirst($row["nome"]); ?></button>
              	  <?php
              						}
              					}
                        $mysqli->close();
                   ?>
                </div>
              </div>
          </div>
          <!-- Risultati -->
          <div class="row">
            <div id="dati" class="container-fluid">
            </div>
          </div>
        </div>
      </main>
    </section>
  </body>
</html>
