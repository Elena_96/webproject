<?php function inserisciSnack($mysqli,$myArray) {
  $email = $_SESSION["email"];
  $myArrayQuant = array();
  $empty = False;
  $format = "col-sm-10 col-10";

  $cartId = getCartId($mysqli, $email);
  if ($cartId < 0) {
    echo "errore";
  }
  foreach ($myArray as $id) {
    if($_POST["p".$id] == 0) {
      $empty = True;
    }
  }
  if (empty($myArray) || $empty == True) {
    return "Attenzione! La quantità non può essere uguale a zero.";
  }else{
    foreach ($myArray as $id) {
      $query = "SELECT codsnack as id, quantita
                FROM inclusioneSnack
                WHERE codsnack = ".$id .
              " AND codCarrello = " . $cartId .
              " AND utente = '" . $email . "'";
      $result = $mysqli->query($query);
        if ($result->num_rows > 0) {
          while ($row = $result->fetch_assoc()) {
            $myArrayQuant[$row["id"]] = $row["quantita"];
          }
     }
    }
    foreach ($myArray as $id) {
      $quant = $_POST["p".$id];
      $prezzo = 0;
      if (array_key_exists($id,$myArrayQuant)) {
        $tot = $quant+$myArrayQuant[$id];
        $stmt = "UPDATE inclusionesnack
                 SET quantita =".$tot.
               " WHERE codsnack =".$id .
               " AND codCarrello = " . $cartId .
               " AND utente = '" . $email . "'";
      } else {
        $stmt = "INSERT INTO inclusionesnack(utente, codcarrello,codsnack, quantita)
                 VALUES ('$email','$cartId','$id','$quant')";
      }
      $query = "SELECT prezzo FROM snack WHERE id = ".$id;
      $result = $mysqli->query($query);
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $prezzo += $row["prezzo"]*$quant;
          }
        }
        if ($mysqli->query($stmt) == TRUE) {
          updateCartTotal($mysqli, $cartId, $email, $prezzo);
          return "";
        } else {
          return $mysqli->error;
      }
    }
  }
}

//Dichiarazione variabili per server
include "utils/functions.php";
include "utils/db_connect.php";
sec_session_start();
$_SESSION["lastPage"] = 'sfiziosita.php';
$myArray = array();

class FoodHeader {
  public $img;
  public $foods;

  function __construct($img,$myFoods) {
    $this->img = $img;
    $this->foods = $myFoods;
  }
}

class Food {
  public $id;
  public $porzione;
  public $quantita;
  public $prezzo;

  function __construct($id,$porzione,$prezzo) {
    $this->id = $id;
    $this->porzione = $porzione;
    $this->quantita = 0;
    $this->prezzo=$prezzo;
  }
}


$conn = connectToDatabase();
$inserito = false;
$msg = "";
if (!empty($_POST['check_list'])) {
  if (!login_check($conn)) {
    header('Location: login.php?error=99');
  }
  $msg = inserisciSnack($conn,$_POST['check_list']);
  $inserito = true;
}

$query_sql="SELECT s.nome as nome,p.nome as porzione,s.id as id,s.prezzo as prezzo, s.img as img
            FROM snack s, porzione p
            WHERE s.codPorzione = p.id
            ORDER BY s.codPorzione";
$result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      $myFood = new Food($row["id"],$row["porzione"],$row["prezzo"]);
      if (!array_key_exists($row["nome"],$myArray)){
        $myFoodHeader = new FoodHeader($row["img"],array());
        $myArray[$row["nome"]]=$myFoodHeader;
      }
        array_push($myArray[$row["nome"]]->foods,$myFood);
    }
 }
?>
 <!DOCTYPE html>
 <html lang="it-IT">
   <head>
     <title>Sfiziosità</title>
     <?php require 'utils/commons.html'; ?>
     <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
     <link rel="stylesheet" href="../css/bev_sfiz.css">
     <script src="../script/bev_sfiz.js"></script>
   </head>
   <body>
     <?php require 'navbar.php' ?>
     <section class="container-fluid page" id="content">
       <div>
         <?php
           $format = "col-sm-10 col-10";
           if($inserito && empty($msg)) {
         ?>
         <div class="row msg">
           <?php echo successOccured($format,"Inserimento completato! Il tuo articolo è stato aggiunto al carrello.");?>
         </div>
         <?php
       } else if($inserito){
         ?>
         <div class="row msg">
           <?php echo errorOccured($format,$msg);?>
         </div>
         <?php
           }
          ?>
       </div>
       <header>
         <h1>Sfiziosità</h1>
         <h2>Gustosi snack da goderti dove e quando vuoi</h2>
       </header>
       <section>
       <?php foreach ($myArray as $key => $value) {?>
         <form action="sfiziosita.php" method="post" class="row">
           <fieldset class="border rounded col-11 mx-auto">
             <div class="container-fluid">
               <div class="row align-items-center">
                 <div class="immagine col-12 col-md-5">
                     <img src= <?php echo getImgFolder().$value->img?> alt="<?php echo $value->img ?>" class="rounded float-left img-fluid"><br/>
                 </div>
                 <div class="col-8 offset-1 offset-md-1 col-md-5">
                  <div class="paragraph">
                    <h3><?php echo $key?></h3>
                    <?php foreach ($value->foods as $tipe => $obj) { ?>
                      <input type="checkbox" id="<?php echo "ch".$obj->id?>" name="check_list[]" value="<?php echo $obj->id?>">
                      <label for="<?php echo $obj->porzione?>">
                        <?php echo $obj->porzione?>&nbsp;&nbsp;<?php echo number_format($obj->prezzo,2);?>€
                      </label><br/>
                      <div class="change <?php echo"ch".$obj->id?>">
                        <div class="arrow">
                          <label>Quantità:</label>
                          <img src="../img/arrow_l.png" alt="Diminuzione quantità" class="img-fluid l <?php echo"size".$obj->id?>">
                          <input type="text" id="<?php echo "p".$obj->id ?>" name = "<?php echo "p".$obj->id?>" value="<?php echo $obj->quantita?>" readonly/>
                          <img src="../img/arrow_r.png" alt="Aumento quantità" class="img-fluid r <?php echo"size".$obj->id?>"><br/>
                        </div>
                     </div>
                    <?php } ?>
                  </div>
                 </div>
                 <div class="col-1 col-md-1 align-self-end carrello">
                   <input type="image" src=<?php echo '"'.getImgFolder().'addToCart.png"'; ?> alt="Icona aggiungi al carrello">
                 </div>
               </div>
             </div>
           </fieldset>
          </form>
          <?php } ?>
       </section>
     </section>
   </body>
 </html>
