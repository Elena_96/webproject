<?php
  include 'utils/functions.php';
  include 'utils/db_connect.php';
  require 'utils/commons.html';
  sec_session_start();
  $conn=connectToDatabase();
  if (login_check($conn)) {
    $sql='SELECT indirizzo
       FROM consegna
       WHERE utente="'.$_SESSION["email"].'"';
    $indirizzo = $conn->query($sql);
    if ($indirizzo->num_rows > 0) {
      $ind = $indirizzo->fetch_assoc();
    }
    $sql="SELECT numero, proprietario
       FROM cartacredito
       WHERE utente='".$_SESSION["email"]."'";
    $carta = $conn->query($sql);
    if ($carta->num_rows > 0) {
      $card = $carta->fetch_assoc();
    }
  } else {
    header("Location: ".$_SESSION["lastPage"]);
  }
 ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Pagamento</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/signIn.css">
    <script src="../script/form.js"> </script>
  </head>
  <body class="container-fluid">
    <header class="container-fluid">
      <div class="mx-auto">
        <img src="../Logo/Logo.png" alt="logo del sito" class="img-fluid"/>
      </div>
    <h1>Pagamento</h1>
    </header>
    <form id="form" action="./pagato.php" method="post">
      <fieldset>
        <legend>Indirizzo di consegna</legend>
          <label for="indirizzo">Indirizzo</label><br/>
          <input id="indirizzo" type="text" name="indirizzo" <?php if ($indirizzo->num_rows > 0) {echo("value='".$ind["indirizzo"]."'");} ?> required><br/>
      </fieldset>

      <fieldset>
        <legend>Carta di credito</legend>
        <label for="intestatario">Intestatario</label><br/>
        <input id="intestatario" type="text" name="intestatario" <?php if ($carta->num_rows > 0) {echo("value='".$card["proprietario"]."'");} ?> required><br/>
        <label for="numeroCarta">Numero della carta</label><br/>
        <input id="numeroCarta"  type="number" name="numeroCarta" <?php if ($carta->num_rows > 0) {echo("value='".$card["numero"]."'");} ?> required><br/>
      </fieldset>

      <div>
        <input id="submit" type="image" src="../img/hand.png" alt="Bottone di invio" class="img-fluid"/>
      </div>
    </form>
  </body>
</html>
