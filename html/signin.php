<?php
    include 'utils/functions.php';
    include 'utils/db_connect.php';
    sec_session_start();
    if (isset($_GET['error'])) {
      $format = "col-sm-10 col-10";
      $res = intval($_GET['error']);?>
      <div>
      <?php
      switch ($res) {
        case 0: echo errorOccured($format, 'Inserire metodi di pagamento o chiudere la sezione!');
                break;
        case -1: echo errorOccured($format, "La seguente email è già collegata ad un account");
                 break;
        case -2: echo errorOccured($format, "Riempire tutti i campi!");
                 break;
        case -3: echo errorOccured($format, "È necessario accettare la normativa della privacy per continuare");
                 break;
        default: echo errorOccured($format, 'Errore non riconosciuto: Error#'.$res);
                 break;
      };
      ?></div><?php
   }
 ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <title>Registrazione</title>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
    <?php require 'utils/commons.html'; ?>
    <link rel="stylesheet" href="../css/signIn.css">
    <link rel="stylesheet" href="../css/switchery.css">
    <script src="../script/form.js"> </script>
    <script src="../script/sha512.js"> </script>
    <script src="../script/switchery.js"></script>
    <script src="../script/signIn.js"></script>
  </head>
  <body class="container-fluid">
    <header class="container-fluid">
      <div class="mx-auto">
        <img src="../Logo/Logo.png" alt="logo del sito" class="img-fluid"/>
      </div>
    <h1>Registrazione</h1>
    </header>
    <form id="form" action="./checkSignIn.php" method="post">
      <fieldset>
        <legend>Generalit&agrave;</legend>
          <label for="nome">Nome</label><br/>
          <input id="nome" type="text" name="nome" placeholder="Paolo" required><br/>
          <label for="cognome">Cognome</label><br/>
          <input id="cognome" type="text" name="cognome" placeholder="Rossi" required><br/>
          <label for="datanascita">Data di nascita</label><br/>
          <input id="datanascita" type="date" name="datanascita" required><br/>
          <label>Sesso</label><br/>
          <input id="maschio" type="radio" name="genere" value="M" checked/><label for="maschio">Uomo</label>
          <input id="femmina" type="radio" name="genere" value="F"/><label for="femmina">Donna</label>
          <input id="altro" type="radio" name="genere" value="A"/><label for="altro">Altro</label>
      </fieldset>

      <fieldset>
        <legend>Indirizzo  &amp; Telefono</legend>
        <label for="indirizzo">Indirizzo di Residenza</label><br/>
        <input id="indirizzo" type="text" name="indirizzo" placeholder="via Tal Dei Tali 6"required><br/>
        <label for="telefono">Telefono</label><br/>
        <input id="telefono"  type="tel" name="telefono" placeholder="+39 *** *** ****" required><br/>
      </fieldset>
      <div class="switch1">
        <input id="switchery" type="checkbox" name="switchery" class="switch" value="1" checked>
      </div>

      <fieldset class="pagamento">
        <legend>Metodo di pagamento</legend>
        <label for="intestatario">Intestatario*</label><br/>
        <input id="intestatario" type="text" name="intestatario" placeholder="Paolo Rossi" ><br/>
        <label for="numerocarta">Numero carta*</label><br/>
        <input id="numerocarta" type="tel" name="numerocarta" placeholder="**** ***** ***** ****"><br/>
        <p>(* I campi sono opzionali. Nel caso non si vogliano compilare si prega di chiudere la sezione.)</p>
      </fieldset>

      <fieldset>
				<legend>Email &amp; Password</legend>
				<label for="email">Email</label><br/>
				<input id="email" type="email" name="email" placeholder="prova@mail.com"required/><br/>
				<label for="password">Password</label><br/>
				<input id="pwd" type="password" name="pwd" placeholder="Password" required/><br/>
  	  </fieldset>

      <fieldset id="informativa_privacy">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" id="privacy" name="privacy" value="accepted">
          <label class="form-check-label" for="privacy">Accetto che i dati inseriti vengano usati secondo i termini della
          protezione e della privacy come indicato al seguente link: <a href="#" target='_blank'> Normativa della privacy <a></label>
        </div>
      </fieldset>

      <div>
        <input id="submit" type="image" src="../img/hand.png" alt="Bottone di invio" class="img-fluid" onclick="formhash('form');"/>
      </div>
    </form>
  </body>
</html>
