<?php
    include 'utils/functions.php';
    include 'utils/db_connect.php';
    sec_session_start();
 ?>

<!DOCTYPE html>
<html lang="it">
  <head>
    <title>Bake and Taste</title>
    <?php
      require 'utils/commons.html';
     ?>
    <link rel="stylesheet" href="../css/header.css">
    <link rel="stylesheet" href="../css/logIn.css">
    <script src="../script/sha512.js"> </script>
    <script src="../script/form.js"> </script>
  </head>
  <body class="container-fluid">
    <?php require 'header.html' ?>
    <main>
      <?php
        if(isset($_GET['error'])) {
        $format = "col-sm-3 col-10";
         $res = intval($_GET['error']);
         switch ($res) {
           case 0:
             echo errorOccured($format, 'Errore durante il log-in!');
             break;
           case -1:
             echo errorOccured($format, 'Hai effettuato il log-in troppe <br/> volte, il tuo account è bloccato temporaneamente.<br/>
                      Riprova più tardi!');
             break;
           case 2:
             echo errorOccured($format, 'Inserire una mail');
             break;
           case 3:
               echo errorOccured($format, 'Inserire la password');
               break;
           case 99:
               echo errorOccured($format, "Per effettuare questa funzionalità è richiesto l'accesso");
               break;
           default:
               echo errorOccured($format, 'Errore non riconosciuto: Error#'.$res);
               break;
         };
        }
      ?>
      <div class="row">
        <form id="form" action="./check.php" method="post" class="mx-auto rounded col-sm-3 col-10">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" id="email" class="form-control" name="email" placeholder="prova@mail.com">
          </div>
          <div class="form-group">
            <label for="pwd">Password</label>
            <input type="password" id="pwd" class="form-control" name="pwd" placeholder="Password">
          </div>
          <div id="img_submit">
            <input id="submit" type="image" src=<?php echo '"'.getImgFolder().'/happy.png"' ?> alt="accedi" onclick="formhash('form');">
            <label for="submit">Accedi</label>
          </div>
        </form>
      </div>
      <div class="row" id="iscriviti">
        <div class="mx-auto text-center">
          <p> Non sei iscritto? Iscriviti subito! </p>
          <a class="btn col-md-7 col-7 btn-outline-primary" href="signin.php" role="button">Iscriviti</a>
        </div>
      </div>
    </main>
  </body>
</html>
