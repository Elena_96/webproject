<?php
  include 'utils/db_connect.php';
  include 'utils/functions.php';
  sec_session_start();
  $carta = false;
if(!isset($_POST["privacy"])) {
  header('Location:signin.php?error=-3');
  exit();
}
if (isset($_POST['email']) && isset($_POST['p']) && isset($_POST['nome']) && isset($_POST['cognome'])
    && isset($_POST['datanascita'])&& isset($_POST['genere']) && isset($_POST['indirizzo'])
    && isset($_POST['indirizzo'])) {
  $email = $_POST['email'];
  $pwd = $_POST['p'];
  $mysqli = connectToDatabase();
  $res = checkEmail($email,$mysqli);
  if ($res) {
    /*se isset è true lo switch è on*/
    if (isset($_POST['switchery']) && $_POST['switchery']== 1) {
      if (strcmp($_POST['intestatario'],"") == 0 || strcmp($_POST['numerocarta'],"") == 0) {
        header('Location:signin.php?error=0');
        exit;
      } else {
        $carta = true;
      }
    }

    $res = signin($email, $pwd, $_POST['nome'],$_POST['cognome'],
           $_POST['datanascita'],$_POST['genere'], $_POST['telefono'], $mysqli);

    if (!$res) {
      header('Location:signin.php?error=1');
      exit;
    }

    if ($carta) {
      $temp = createCarta($email, $_POST['intestatario'], $_POST['numerocarta'], $mysqli);
      if (!$temp) {
        header('Location:signin.php?error=2');
        exit;
      }
    }

    $carrello = createCart($email, $mysqli);

    if (!$carrello) {
      header('Location:signin.php?error=3');
    }

    $consegna = createConsegna($email, $_POST['indirizzo'], $mysqli);

    if (!$consegna) {
      header('Location:signin.php?error=4');
    }
    ?>
    <form id="myForm" action="check.php" method="post">
    <?php
      echo '<input type="hidden" name="'.htmlentities('email').'" value="'.htmlentities($email).'">';
      echo '<input type="hidden" name="'.htmlentities('p').'" value="'.htmlentities($pwd).'">';
    ?>
    </form>
    <script type="text/javascript">
      document.getElementById('myForm').submit();
    </script>
    <?php
    } else {
      header('Location:signin.php?error=-1');
    }
  } else {
    header('Location:signin.php?error=-2');
  }
?>
