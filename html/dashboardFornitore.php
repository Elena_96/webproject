<?php
		require 'utils/functions.php';
		require 'utils/db_connect.php';
		require 'func_page/dashboardFornitore_func.php';

		$mysqli = connectToDatabase();
		$userCount = 0;
		$userData = getUserData($mysqli);
		if (!empty($userData)) {
			$userCount = $userData[0];
		}
		$cartData = getCartData($mysqli);
		$menuData = getMenuData($mysqli, 5);
		$burgerData = getBurgerData($mysqli, 5);
		$snackData = getSnackData($mysqli, 5);
		$drinkData = getDrinkData($mysqli, 5);
		$mysqli->close();
?>
<!DOCTYPE HTML>
<html>
<head>
	<script>

		function getChart(element, chartType, dataChart) {
			return new Chart(element, {
		      type: chartType,
		      data: dataChart,
		  });
		}

		function setUpFirstRow() {
			<?php if($userCount > 0) {?>
			var chart1 = document.getElementById("sexChart").getContext('2d');
			var chart2 = document.getElementById("ageChart").getContext('2d');
			var data = {
							labels: ["Maschio", "Femmina"],
							datasets: [{
									data: [<?php echo $userData[2] . "," . $userData[3]; ?>],
									backgroundColor: [
										'rgba(54, 162, 235, 0.2)',
										'rgba(255, 99, 132, 0.2)',
									],
									borderColor: [
										'rgba(54, 162, 235, 1)',
										'rgba(255,99,132,1)',
									],
									borderWidth: 1
							}],
			};
			var sexChart = getChart(chart1, "doughnut", data);
			data = {
					labels: [
					<?php
					  $keys = array_keys($userData[4]);
						$keys_length = count($keys);
						for($i = 0; $i < $keys_length - 1; $i ++) {
							echo '"' . $keys[$i] . "-" . $keys[$i + 1] . '"' . ",";
						}
						echo '"over ' . $keys[$i] . '"';
					?> ],
					datasets: [{
							data: [
								<?php
									foreach ($userData[4] as $key => $value) {
										echo $value . ",";
									}
								?>
							],
							backgroundColor: [
									'rgba(255, 0, 255, 0.2)', //fucsia
									'rgba(255, 140, 0, 0.2)', //arancione
									'rgba(127, 255, 0, 0.2)', //verde
									'rgba(0, 255, 255, 0.2)', //ciano
									'rgba(255, 0, 0, 0.2)'  , // rosso
									'rgba(255, 255, 0, 0.2)', // giallo
							],
							borderColor: [
								'rgba(255, 0, 255, 0.2)', //fucsia
								'rgba(255, 140, 0, 0.2)', //arancione
								'rgba(127, 255, 0, 0.2)', //verde
								'rgba(0, 255, 255, 0.2)', //ciano
								'rgba(255, 0, 0, 0.2)'  , // rosso
								'rgba(255, 255, 0, 0.2)', // giallo
							],
							borderWidth: 1
					}],
			};
			var ageChart = getChart(chart2, "pie", data);
			<?php } ?>
		}

		function setUpSecondRow() {
			<?php if(!empty($menuData)) {?>
			var chart1 = document.getElementById("menuChart").getContext('2d');
			var data = {
							labels: [
								<?php
								  $keys = array_keys($menuData);
									$keys_length = count($keys);
									for($i = 0; $i < $keys_length; $i ++) {
										echo '"' . $keys[$i] . '"' . ",";
									}
								?>
							],
							datasets: [{
									data: [
										<?php
											foreach ($menuData as $key => $value) {
												echo $value . ",";
											}
										?>
									],
									backgroundColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderWidth: 1
							}],
			};
			var menuChart = getChart(chart1, "doughnut", data);
			<?php } ?>
		}

		function setUpThirdRow() {
			var chart1 = document.getElementById("burgerChart").getContext('2d');
			var chart2 = document.getElementById("snackChart").getContext('2d');
			<?php if(!empty($burgerData)) {?>
			var data = {
							labels: [
								<?php
									$keys = array_keys($burgerData);
									$keys_length = count($keys);
									for($i = 0; $i < $keys_length; $i ++) {
										echo '"' . $keys[$i] . '"' . ",";
									}
								?>
							],
							datasets: [{
									data: [
										<?php
											foreach ($burgerData as $key => $value) {
												echo $value . ",";
											}
										?>
									],
									backgroundColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderWidth: 1
							}],
			};
			var burgerChart = getChart(chart1, "doughnut", data);
			<?php }
			if (!empty($snackData)) {?>
			data = {
							labels: [
								<?php
									$keys = array_keys($snackData);
									$keys_length = count($keys);
									for($i = 0; $i < $keys_length; $i ++) {
										echo '"' . $keys[$i] . '"' . ",";
									}
								?>
							],
							datasets: [{
									data: [
										<?php
											foreach ($snackData as $key => $value) {
												echo $value . ",";
											}
										?>
									],
									backgroundColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderWidth: 1
							}],
			};
			var snackChart = getChart(chart2, "doughnut", data);
			<?php } ?>
		}

		function setUpFourthRow() {
			<?php if(!empty($drinkData)) {?>
			var chart1 = document.getElementById("drinkChart").getContext('2d');
			var data = {
							labels: [
								<?php
									$keys = array_keys($drinkData);
									$keys_length = count($keys);
									for($i = 0; $i < $keys_length; $i ++) {
										echo '"' . $keys[$i] . '"' . ",";
									}
								?>
							],
							datasets: [{
									data: [
										<?php
											foreach ($drinkData as $key => $value) {
												echo $value . ",";
											}
										?>
									],
									backgroundColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderColor: [
										'rgba(255, 0, 255, 0.2)', //fucsia
										'rgba(255, 0, 0, 0.2)'  , // rosso
										'rgba(127, 255, 0, 0.2)', //verde
										'rgba(0, 255, 255, 0.2)', //ciano
										'rgba(255, 255, 0, 0.2)', // giallo
									],
									borderWidth: 1
							}],
			};
			var drinkChart = getChart(chart1, "doughnut", data);
			<?php } ?>
		}

		$(document).ready(function(){
		  setUpFirstRow();
			setUpSecondRow();
			setUpThirdRow();
			setUpFourthRow();
			$(".dashRiga .col-4").addClass("d-flex align-items-stretch");
			$(".dashRiga .card-body").addClass("d-flex align-items-center");
			$(".dashRiga .card").addClass("text-center border border-dark");
			$(".dashRiga .card-header").addClass("border-dark bg-info text-white");
			$(".dashRiga .card-footer").addClass("border-dark bg-info text-white");
			// tutte le card devono avere lunghezza uguale
			var maxWidth = 0;
			for (var card in $(".dashRiga .card")) {
				if ($(card).width() > maxWidth) {
					maxWidth = $(card).width();
				}
			}
			$(".card").width(maxWidth);
		});
	</script>
</head>
<body>
	<div class="row dashRiga"> <!-- prima riga -->
		<div class="col-1"> </div>
		<div class="col-4">
			<div class="card">
				<div class="card-header">
					utenti iscritti
				</div>
				<div class="card-body">
					<canvas id="sexChart"></canvas>
				</div>
				<div class="card-footer">
					Totale : <?php echo $userCount; ?>
				</div>
			</div>
		</div>
		<div class="col-2"> </div>
		<div class="col-4">
			<div class="card">
				<div class="card-header">
					età iscritti
				</div>
				<div class="card-body">
					<canvas id="ageChart"></canvas>
				</div>
				<div class="card-footer">
					Età media : <?php if($userCount > 0) { echo number_format( $userData[1], 2, ",", "."); }
					                  else               { echo "0"; } ?>
				</div>
			</div>
		</div>
		<div class="col-1"> </div>
	</div>
	<div class="row dashRiga"> <!-- seconda riga -->
		<div class="col-1"> </div>
		<div class="col-4">
			<div class="card">
				<div class="card-header">
					acquisti effettuati
				</div>
				<div class="card-body">
					<div id="infoAcquisti" class="mx-auto">
						Totale acquisti: <?php echo $cartData[0]; ?> <br/>
						Somma totale acquisti: <?php echo number_format( $cartData[1], 2, ",", "."); ?>€ <br/>
						Spesa maggiore:
						<?php
							echo $cartData[2];
							if ($cartData[2] > 0) {
						 ?>€ effettuata da
						<?php
							echo $cartData[3];
							}
						 ?>
					</div>
				</div>
				<div class="card-footer">
					Nell'ultimo mese: <?php echo $cartData[4] ?>
				</div>
			</div>
		</div>
		<div class="col-2"> </div>
		<div class="col-4">
			<div class="card">
				<div class="card-header">
					menù più venduti
				</div>
				<div class="card-body">
					<canvas id="menuChart"></canvas>
				</div>
			</div>
		</div>
		<div class="col-1"> </div>
	</div>
	<div class="row dashRiga"> <!-- terza riga -->
		<div class="col-1"> </div>
		<div class="col-4">
			<div class="card">
				<div class="card-header">
					panini più venduti
				</div>
				<div class="card-body">
					<canvas id="burgerChart"></canvas>
				</div>
			</div>
		</div>
		<div class="col-2"> </div>
		<div class="col-4">
			<div class="card">
				<div class="card-header">
					sfiziosità più vendute
				</div>
				<div class="card-body">
					<canvas id="snackChart"></canvas>
				</div>
			</div>
		</div>
		<div class="col-1"> </div>
	</div>
	<div class="row dashRiga"> <!-- quarta riga -->
		<div class="col-4"> </div>
		<div class="col-4">
			<div class="card">
				<div class="card-header">
					bevande più vendute
				</div>
				<div class="card-body">
					<canvas id="drinkChart"></canvas>
				</div>
			</div>
		</div>
		<div class="col-4"> </div>
	</div>
</body>
</html>
