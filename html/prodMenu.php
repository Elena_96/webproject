<?php
include "utils/functions.php";
include "utils/db_connect.php";
sec_session_start();
$_SESSION["lastPage"] = 'prodMenu.php';
$myArray = array();

function inserisciMenu($mysqli,$myArray) {
  $email = $_SESSION["email"];
  $cartId = getCartId($mysqli, $email);
  $format = "col-sm-10 col-10";
  $prezzo = 0;

  if ($cartId < 0) {
    echo "errore";
  }

  $id = $_POST["idMenu"];
  $quant = $_POST["p".$id];
  if ($quant == 0) {
      return "Attenzione! La quantità non può essere uguale a zero.";
  } else {
    $quantitaGiaPresente = 0;
    $query1 = "SELECT quantita
               FROM inclusioneMenu
               WHERE codmenu = ".$id .
             " AND codCarrello = " . $cartId .
             " AND utente = '" . $email . "'";
    $result = $mysqli->query($query1);

    if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $quantitaGiaPresente += $row["quantita"];
      }
    }

    if ($quantitaGiaPresente > 0) {
      $tot = $quantitaGiaPresente + $quant;
      $stmt = "UPDATE inclusioneMenu
               SET quantita =".$tot.
             " WHERE codmenu =".$id .
             " AND codCarrello = " . $cartId .
             " AND utente = '" . $email . "'";
    } else {
      $stmt = "INSERT INTO inclusioneMenu(utente, codcarrello,codmenu,quantita)
                VALUES ('$email', '$cartId', '$id','$quant')";
    }

    $query = "SELECT prezzo FROM menu WHERE id = ".$id;
    $result = $mysqli->query($query);

    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $prezzo += $row["prezzo"]*$quant;
      }
    }

    if ($mysqli->query($stmt) == TRUE) {
      updateCartTotal($mysqli, $cartId, $email, $prezzo);
      return "";
    } else {
      return $mysqli->error;
    }
  }
}

class Menu {
  public $prezzo;
  public $sconto;
  public $quantita;
  public $nome;
  public $id;
  public $img;

  function __construct($id,$nome,$prezzo,$sconto,$img,$myFoods) {
    $this->prezzo = $prezzo;
    $this->sconto = $sconto;
    $this->foods = $myFoods;
    $this->nome = $nome;
    $this->quantita = 0;
    $this->id = $id;
    $this->img = $img;
  }
}

class MenuFood {
  public $bevanda;
  public $panino;
  public $snack;
  public $snackporz;
  public $bevporz;

  function __construct($panino,$snack,$bevanda,$bevporz,$snackporz) {
    $this->bevanda = $bevanda;
    $this->panino = $panino;
    $this->snack = $snack;
    $this->snackporz = $snackporz;
    $this->bevporz = $bevporz;
  }
}

$conn = connectToDatabase();
$inserito = false;
$msg = "";
if (!empty($_POST["idMenu"])) {
  if (!login_check($conn)) {
    header('Location: login.php?error=99');
  }
  $msg = inserisciMenu($conn,$_POST['idMenu']);
  $inserito = true;
}

$query_sql="SELECT m.id as idM, m.nome as nomeM, m.prezzo as prezzoM, m.sconto as scontoM, m.img as imgM,
            b.nome as nomeb, s.nome as nomes, p.nome as nomep,
            snackporz.nome as snackporz, bevporz.nome as bevporz
            FROM menu m, presenzabevanda pb,bevanda b, presenzasnack ps, snack s,
            prodottostandard p, presenzaprodstand pps, porzione snackporz, porzione bevporz
            WHERE b.id = pb.codbevanda
            AND s.id = ps.codsnack
            AND p.id = pps.codprodstand
            AND b.codPorzione = bevporz.id
            AND s.codPorzione = snackporz.id
            AND m.id = pb.codmenu
            AND m.id = ps.codmenu
            AND m.id = pps.codmenu
            GROUP BY m.id,m.nome,m.prezzo,m.sconto,m.img
            ORDER BY m.id";

$result = $conn->query($query_sql);

  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      $myMenuFood = new MenuFood($row["nomep"],$row["nomes"],$row["nomeb"],$row["bevporz"],$row["snackporz"]);
     if (!array_key_exists($row["idM"],$myArray)){
       $myMenu = new Menu($row["idM"],$row["nomeM"],$row["prezzoM"],$row["scontoM"],$row["imgM"],array());
       $myArray[$row["idM"]]=$myMenu;
     }
       array_push($myArray[$row["idM"]]->foods,$myMenuFood);
   }
 }
?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <title>Menù</title>
    <?php require 'utils/commons.html'; ?>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/prodMenu.css">
    <script src="../script/prodotti.js"></script>
  </head>
  <body>
    <?php require 'navbar.php' ?>
    <section class="container-fluid page" id="content">
      <div>
        <?php
          $format = "col-sm-10 col-10";
          if($inserito && empty($msg)) {
        ?>
        <div class="row msg">
          <?php echo successOccured($format,"Inserimento completato! Il tuo articolo è stato aggiunto al carrello.");?>
        </div>
        <?php
      } else if($inserito){
        ?>
        <div class="row msg">
          <?php echo errorOccured($format,$msg);?>
        </div>
        <?php
          }
         ?>
      </div>
      <header>
        <h1>Menù</h1>
        <h2>Quando la fame è MAXI</h2>
      </header>
      <section>
        <?php foreach ($myArray as $key => $value) {?>
          <form action="prodMenu.php" method="post" class="row">
            <fieldset class="border rounded col-11 mx-auto">
                <div class="container-fluid">
                  <div class="row align-items-center">
                    <div class="col-12 col-md-5 immagine ">
                      <img src= <?php echo getImgFolder().$value->img?> alt="<?php echo $value->nome?>" class="rounded float-left img-fluid"><br/>
                    </div>
                    <div class="col-8 offset-1 offset-md-1 col-md-5">
                      <div class="paragraph">
                        <h3><?php echo $value->nome ?></h3>
                          <p>
                            <?php foreach ($value->foods as $tipe => $obj) { ?>
                              Contiene:</p>
                            <ul>
                              <li>Panino:<?php echo $obj->panino?></li>
                              <li>Snack:<?php echo $obj->snack?></li>
                                  porzione: <?php echo $obj->snackporz?>
                              <li>Bevanda:<?php echo $obj->bevanda?></li>
                                  porzione: <?php echo $obj->bevporz?>
                            </ul>
                            <?php } ?>
                          <p>Prezzo:<?php echo number_format($value->prezzo,2)?>€ </br>
                        </p>
                        <div class="change">
                          <div class="arrow">
                            <img src="../img/arrow_l.png" alt="Diminuzione quantità" class="img-fluid l <?php echo"size".$value->id?>">
                             <input type="text" id="<?php echo "p".$value->id ?>" name = "<?php echo "p".$value->id?>" value="<?php echo $value->quantita?>" readonly/>
                             <img src="../img/arrow_r.png" alt="Aumento quantità" class="img-fluid r <?php echo"size".$value->id?>"><br/>
                          </div>
                          <input type="hidden" name="idMenu" value="<?php echo $value->id?>">
                         </div>
                      </div>
                    </div>
                    <div class="col-1 col-md-1 align-self-end carrello">
                      <input type="image" src=<?php echo '"'.getImgFolder().'addToCart.png"'; ?> alt="Icona aggiungi al carrello">
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
        <?php } ?>
      </section>
    </section>
  </body>
</html>
