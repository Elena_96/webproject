<?php
  include 'utils/db_connect.php';
  if(isset($_POST['p'])) {
    $email = $_POST['email'];
    $password = $_POST['p'];
    $fornitore = 0;
    $nome = $_POST['nome'];
    $cognome = $_POST['cognome'];
    $data = date("Y-m-d", strtotime($_POST['data']));
    $sesso = $_POST['sesso'];
    $telefono = $_POST['telefono'];
    // Crea una chiave casuale
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    // Crea una password usando la chiave appena creata.
    $password = hash('sha512', $password.$random_salt);
    // Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
    // Assicurati di usare statement SQL 'prepared'.
    $mysqli = connectToDatabase();
    if ($insert_stmt = $mysqli->prepare("INSERT INTO utente (user, pwd, nome, cognome, sesso, fornitore, dataNascita, numeroTelefono, codice) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
     $insert_stmt->bind_param('sssssssss', $email, $password, $nome, $cognome,$sesso,$fornitore,$data,$telefono, $random_salt);
     // Esegui la query ottenuta.
     $insert_stmt->execute();
    }
    $mysqli->close();
  }
 ?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title></title>
     <script src="../script/form.js"> </script>
     <script src="../script/sha512.js"> </script>
   </head>
   <body>
     <form id="form" class="" action="provaRegistrazione.php" method="post">
       <input type="email" name="email" value="francesco.grandinett@gmail.com">
       <input type="password" id="pwd" name="password" value="casa">
       <input type="text" name="nome" value="Francesco">
       <input type="text" name="cognome" value="Grandinetti">
       <input type="text" name="sesso" value="M">
       <input type="date" name="data">
       <input type="tel" name="telefono" value="3474864891">
       <input type="submit" onclick="formhash('form');">
     </form>
   </body>
 </html>
