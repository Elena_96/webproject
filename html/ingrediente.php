<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>

    <?php
    include 'utils/db_connect.php';
    include 'utils/functions.php';
    //accetto solo richieste GET con q settato
    if (isset($_GET['q'])) {
      $q = intval($_GET['q']);
      // prendo tutti gli ingredienti del tipo che mi è stato richiesto con la GET
      $mysqli = connectToDatabase();
      $sql = "SELECT i.*, t.nome AS 'tipo' FROM ingrediente i, tipoIngrediente t WHERE t.id = i.codTipo AND i.codTipo = ".$q." ORDER BY codTipo, i.id";
      $res = $mysqli->query($sql);
      if ($res->num_rows > 0) {
        $tipo = null;
            while($row = $res->fetch_assoc()) {
              if ($row["tipo"] != $tipo) {
                if(!is_null($tipo)) {
       ?>
          </div>
        </div>
      <?php
                }
                $tipo = $row["tipo"];
      ?>
        <div id="<?php echo $tipo; ?>" class="row"> <!-- Contenitore del tipo-->
          <div class="container-fluid">
     <?php
             }
             $id = $tipo.'_'.$row["id"];
      ?>
         <div id="<?php echo $id; ?>" class="row ingrediente">
             <div class="col-4 col-md-3">
                  <img src="<?php echo getImgFolder()."/immagini/".$row["img"]; ?>" alt="immagine relativa al prodotto" class="img-fluid mx-auto d-block">
             </div>
             <div class="col-6 col-md-7 dettagli">
               <h2><?php echo $row["nome"]; ?></h2>
             </div>
             <div class="col-2 aggiungi text-center">
               <em class="fa fa-plus-circle" onclick="aggiungi('#<?php echo $id ?>','<?php echo $row["id"]?>',<?php echo $row["codTipo"] ?>,'<?php echo $row["tipo"]?>');"></em>
               <span><?php echo number_format($row["prezzo"],2).'€'; ?></span>
           </div>
         </div>
     <?php
            }
          }
          $mysqli->close();
    } else {
      echo "Invalid Request";
    }
    ?>
  </body>
</html>
