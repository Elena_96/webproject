<?php

  /**
   * Effettua la connessione al database; ritorna l'oggetto mysqli con cui interagire.
   **/
  function connectToDatabase() {
    $host = "localhost"; // E' il server a cui ti vuoi connettere.
    $user = "admin_writer"; // E' l'utente con cui ti collegherai al DB.
    $pwd = "Bzz8zRzvMh34Rt"; // Password di accesso al DB.
    $database = "bakeandtaste"; // Nome del database.
    return new mysqli($host, $user, $pwd, $database);
  }

  /*
   * Credenziali amministratore:
   * user: adminSupremo
   * pwd: iosonogomorra
   */

  /**
   * Metodo da usare per ottenere il carrello dell'utente passato come parametro.
   * Ritorna l'identificatore del carrello da utilizzare.
   **/
  function getCartId($mysqli, $user_id) {
    $res = $mysqli->prepare("SELECT id FROM carrello WHERE utente = ? AND dataAcquisto IS NULL");
    $res->bind_param('s', $user_id);
    $res->execute();
    $res->store_result();
    $res->bind_result($cartId);
    $res->fetch();
    if ($res->num_rows == 1) {
      return $cartId;
    } else {
      return -1;
    }
  }

  /**
   * Metodo da usare per aggiornare il prezzo totale del carrello.
   **/
  function updateCartTotal($mysqli, $cartId, $user_id, $objPrice) {
    $res = $mysqli->prepare("SELECT prezzototale FROM carrello WHERE id = ? AND utente = ?");
    $res->bind_param("is", $cartId, $user_id);
    $res->execute();
    $res->store_result();
    $res->bind_result($price);
    $res->fetch();
    if ($res->num_rows < 1) {
      $price = 0;
    }
    $price += $objPrice;
    $sql = "UPDATE carrello SET prezzototale=".$price." WHERE id=".$cartId." AND utente='".$user_id."'";
    $mysqli->query($sql);
  }

  /**
   * Effettua il login sul database dati i parametri email e password; ritorna:
   * - (-1) se l'utente è bloccato e non può più collegarsi per due ore
   * -  (0) se l'utente non esiste (pwd sbagliata = utente non esistente)
   * -  (1) se il login ha avuto successo
   **/
  function login($email, $password, $mysqli) {
    // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
    if ($res = $mysqli->prepare("SELECT pwd, codice FROM utente WHERE user = ? LIMIT 1")) {
      $res->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $res->execute(); // esegue la query appena creata.
      $res->store_result();
      $res->bind_result($pwd, $codice); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $res->fetch();
      $password = hash('sha512', $password.$codice); // codifica la password usando una chiave univoca.
      $user_id = $email;
      if($res->num_rows == 1) { // se l'utente esiste
        // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
        if(checkbrute($user_id, $mysqli) == true) {
          // Account bloccato, non si può continuare ad accedere
          return -1;
        } else {
          if($pwd == $password) { // Password corretta!
            $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.
            $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
            // Login eseguito con successo.
            return 1;
          } else { // Password incorretta.
            // Registriamo il tentativo fallito nel database.
            $now = time();
            $mysqli->query("INSERT INTO tentativoLogin (user_id, time) VALUES ('$user_id', '$now')");
            if(checkbrute($user_id, $mysqli) == true) {
              return -1; //avvisi che si sono fatti troppi errori
            } else {
              return 0; //L'utente inserito non esiste.
            }
          }
        }
      } else {
        // L'utente inserito non esiste.
        return 0;
      }
    }
  }

  function isAdmin($email, $mysqli) {
    if ($res = $mysqli->prepare("SELECT fornitore FROM utente WHERE user = ? LIMIT 1")) {
      $res->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $res->execute(); // esegue la query appena creata.
      $res->store_result();
      $res->bind_result($fornitore); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $res->fetch();
      return $fornitore ? true : false;
    }
    return 0;
  }

  function setData($mysqli, $email) {
    if ($res = $mysqli->prepare("SELECT nome, cognome, fotoprofilo FROM utente WHERE user = ?")) {
        $res->bind_param('s', $email); // esegue il bind del parametro '$email'.
        $res->execute(); // esegue la query appena creata.
        $res->store_result();
        $res->bind_result($nome, $cognome, $foto); // recupera il risultato della query e lo memorizza nelle relative variabili.
        $res->fetch();
        $_SESSION["utente"] = $nome." ".$cognome;
        $_SESSION["email"] = $email;
        $_SESSION["foto"] = $foto;
    }
  }

  /**
   * Effettua un controllo sull'utente passato come parametro per verificare se
   * ha effettuato troppi tentativi di accesso al suo profilo nelle ultime due ore.
   * Ritorna:
   * - true se sono stati fatti troppi tentativi
   * - false altrimenti
   **/
  function checkbrute($user_id, $mysqli) {
     // Recupero il timestamp
     $now = time();
     // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
     $valid_attempts = $now - (2 * 60 * 60);
     if ($stmt = $mysqli->prepare("SELECT time FROM tentativoLogin WHERE user_id = ? AND time > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);
        // Eseguo la query creata.
        $stmt->execute();
        $stmt->store_result();
        // Verifico l'esistenza di più di 5 tentativi di login falliti.
        if($stmt->num_rows > 5) {
           return true;
        } else {
           return false;
        }
     }
  }

  /**
   * Metodo da utilizzare per verificare se l'utente ha effettuato il login.
   **/
  function login_check($mysqli) {
     // Verifica che tutte le variabili di sessione siano impostate correttamente
     if(isset($_SESSION['login_string'])) {
       $user_id = $_SESSION['email'];
       $login_string = $_SESSION['login_string'];
       $user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente
       if ($res = $mysqli->prepare("SELECT pwd FROM utente WHERE user = ?")) {
          $res->bind_param('s', $user_id); // esegue il bind del parametro '$user_id'.
          $res->execute(); // Esegue la query creata.
          $res->store_result();
          if($res->num_rows == 1) { // se l'utente esisteecho "login ok";
             $res->bind_result($password); // recupera le variabili dal risultato ottenuto.
             $res->fetch();
             $login_check = hash('sha512', $password.$user_browser);
             if($login_check == $login_string) {
                // Login eseguito!!!!
                return true;
             }
          }
       }
     }
     return false;
  }
  function signin($email, $password,$nome,$cognome,$datanascita,$genere,$telefono,$mysqli){
    $data = date("Y-m-d", strtotime($datanascita));
    $fornitore = 0;
    // Crea una chiave casuale
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    // Crea una password usando la chiave appena creata.
    $password = hash('sha512', $password.$random_salt);
    if ($insert_stmt = $mysqli->prepare("INSERT INTO utente (user, pwd, nome, cognome, sesso, fornitore, dataNascita, numeroTelefono, codice) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
      $insert_stmt->bind_param('sssssssss', $email, $password, $nome, $cognome,$genere,$fornitore,$data,$telefono, $random_salt);
      // Esegui la query ottenuta.
      $insert_stmt->execute();
      return true;
    }
    return false;
}

function createCarta($email,$intestatario,$numerocarta,$mysqli){
  if ($insert_stmt = $mysqli->prepare("INSERT INTO cartacredito (numero,proprietario,utente) VALUES (?, ?, ?)")) {
  $insert_stmt->bind_param('sss', $numerocarta, $intestatario, $email);
  // Esegui la query ottenuta.
  $insert_stmt->execute();
  return true;
  }
  return false;
}

function createCart($email,$mysqli){
if ($res = $mysqli->prepare("SELECT MAX(id) id FROM carrello WHERE utente = ?")) {
    $res->bind_param('s', $email); // esegue il bind del parametro '$email'.
    $res->execute(); // esegue la query appena creata.
    $res->store_result();
    $res->bind_result($id); // recupera il risultato della query e lo memorizza nelle relative variabili.
    $res->fetch();
    if (is_null($id)) {
      $id=0;
    } else {
      $id=$id+1;
    }
  } else {
    return false;
  }
  $data = NULL;
  $tot = 0;
  if ($insert_stmt = $mysqli->prepare("INSERT INTO carrello (utente, id, dataacquisto, prezzototale) VALUES (?, ?, ?, ?)")) {
  $insert_stmt->bind_param('sisi', $email, $id, $data, $tot);
  // Esegui la query ottenuta.
  $insert_stmt->execute();
  return true;
  }
  return false;
}


function payCart($spesa, $CartId, $ind, $mysqli){
  $email=$_SESSION["email"];
  if ($pay = $mysqli->prepare("UPDATE carrello SET dataacquisto=CURRENT_DATE(), prezzototale=?, indirizzo=? WHERE id=? AND utente=?")) {
    $pay->bind_param('dsis',$spesa, $ind, $CartId, $_SESSION["email"]); // esegue il bind dei parametri '$spesa, $CartId '.
    $pay->execute(); // esegue la query appena creata.
    if ($insert_stmt = $mysqli->prepare("INSERT INTO `notifica`( `destinatario`, `messaggio`, `visto`, `mittente`, `codCarrello`) VALUES ('adminSupremo', 'Ordine arrivato', false, ?, ?)")) {
      $insert_stmt->bind_param('si', $email, $CartId);
      // Esegui la query ottenuta.
      $insert_stmt->execute();
      return true;
    }
    return false;
  }
  return false;
}



function checkEmail($email,$mysqli){
  if ($res = $mysqli->prepare("SELECT user FROM utente WHERE user = ?")) {
      $res->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $res->execute(); // esegue la query appena creata.
      $res->store_result();
      $res->bind_result($id); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $res->fetch();
      if(is_null($id)){
      return true;
      }
    }
  return false;
}

function createConsegna($email,$indirizzo,$mysqli){
  if ($insert_stmt = $mysqli->prepare("INSERT INTO consegna (indirizzo,utente) VALUES (?, ?)")) {
  $insert_stmt->bind_param('ss', $indirizzo, $email);
  // Esegui la query ottenuta.
  $insert_stmt->execute();
  return true;
  }
  return false;
}

  function check_logout() {
    if (isset($_POST['logout'])) {
      unset($_SESSION['login_string']);
     }
   }
 ?>
