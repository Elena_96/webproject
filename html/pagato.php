<?php
include 'utils/functions.php';
include 'utils/db_connect.php';
sec_session_start();

$conn=connectToDatabase();
$login=login_check($conn);
if ($login && isset($_POST["indirizzo"]) && isset($_POST["intestatario"]) && isset($_POST["numeroCarta"])) {
  $spesa=0;
  $CartId=getCartId($conn, $_SESSION['email']);

  $query_sql="SELECT i.quantita as quantita, m.prezzo as prezzo
              FROM inclusionemenu i, menu m
              WHERE i.codcarrello = ".$CartId." AND i.codmenu = m.id AND i.utente='".$_SESSION['email']."'" ;

  $result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $spesa=$spesa+$row["prezzo"]*$row["quantita"];
    }
  }

  $query_sql="SELECT i.quantita as quantita, p.prezzo as prezzo
               FROM inclusioneprodstand i, prodottostandard p
               WHERE i.codcarrello = ".$CartId." AND i.codProdStand = p.id AND i.utente='".$_SESSION['email']."'" ;

  $result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $spesa=$spesa+$row["prezzo"]*$row["quantita"];
    }
  }

  $query_sql="SELECT p.prezzofinale as prezzo
                FROM prodottopersonalizzato p
                WHERE p.codcarrello = ".$CartId." AND p.utente='".$_SESSION['email']."'";

  $result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $spesa=$spesa+$row["prezzo"];
    }
  }

  $query_sql="SELECT i.quantita as quantita, s.prezzo as prezzo
              FROM inclusionesnack i, snack s
              WHERE i.codcarrello = ".$CartId." AND i.codsnack = s.id AND i.utente='".$_SESSION['email']."'";

  $result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $spesa=$spesa+$row["prezzo"]*$row["quantita"];
    }
  }

  $query_sql="SELECT i.quantita as quantita, b.prezzo as prezzo
               FROM inclusionebevanda i, bevanda b
               WHERE i.codcarrello = ".$CartId." AND i.codbevanda = b.id AND i.utente='".$_SESSION['email']."'";
  $result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $spesa=$spesa+$row["prezzo"]*$row["quantita"];
    }
  }
  if ($spesa!=0) {
    payCart($spesa, $CartId, $_POST["indirizzo"], $conn);
    createCart($_SESSION["email"],$conn);
    header("Location: ".$_SESSION["lastPage"]);
  } else {
    header("Location: ".$_SESSION["lastPage"]);
  }
} else {
  header("Location: ".$_SESSION["lastPage"]);
}



?>
