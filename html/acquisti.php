<?php
  include 'utils/functions.php';
  include 'utils/db_connect.php';
  sec_session_start();
  check_logout();

  $_SESSION["lastPage"]="acquisti.php";

  class Cart {
    public $id;
    public $spesa;
    public $data;
    public $item;

    function __construct($id,$spesa,$data,$item)
    {
      $this->id=$id;
      $this->spesa=$spesa;
      $this->data=$data;
      $this->item=$item;
    }
  }

  class Item {
    public $id;
    public $quantita;
    public $prezzo;
    public $nome;
    public $porzione;
    public $sconto;
    public $descrizione;

    function __construct($id,$nome,$prezzo,$sconto,$quantita,$descrizione,$porzione,$tipologia)
    {
      $this->id = $tipologia."-".$id;
      $this->nome = $nome;
      $this->quantita = $quantita;
      $this->porzione = $porzione;
      $this->prezzo = $prezzo;
      $this->sconto = $sconto;
      $this->descrizione = $descrizione;
    }
  }

  $conn=connectToDatabase();
  $login=login_check($conn);
  if ($login) {
    $Carrelli=array();
    $sql="SELECT id, dataacquisto, prezzototale FROM `carrello` WHERE `utente`='".$_SESSION["email"]."' AND NOT `dataacquisto`='NULL' ORDER BY `id` DESC LIMIT 10";
    $res=$conn->query($sql);
    if ($res->num_rows>0) {
      while($row = $res->fetch_assoc()) {
        $CartId=$row["id"];
        $spesatemp=$row["prezzototale"];
        $datatemp=$row["dataacquisto"];

        $ItemInCart=array();

        $query_sql="SELECT m.nome as nome, i.quantita as quantita, m.id as id,m.prezzo as prezzo, m.sconto as sconto
                    FROM inclusionemenu i, menu m
                    WHERE i.codcarrello = ".$CartId." AND i.codmenu = m.id AND i.utente='".$_SESSION['email']."'" ;

        $result = $conn->query($query_sql);
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $myFood = new Item($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],"","","menu");
            $ItemInCart[]=$myFood;
          }
        }

        $query_sql="SELECT p.nome as nome, i.quantita as quantita, p.id as id,p.prezzo as prezzo, p.sconto as sconto, p.descrizione as descrizione
                     FROM inclusioneprodstand i, prodottostandard p
                     WHERE i.codcarrello = ".$CartId." AND i.codProdStand = p.id AND i.utente='".$_SESSION['email']."'" ;

        $result = $conn->query($query_sql);
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
             $myFood = new Item($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],$row["descrizione"],"","prodStand");
             $ItemInCart[]=$myFood;
          }
        }

        $query_sql="SELECT p.id as id, p.prezzofinale as prezzo, t.nome as nome
                      FROM prodottopersonalizzato p, tipoprodotto t
                      WHERE p.codcarrello = ".$CartId." AND p.codTipo = t.id AND p.utente='".$_SESSION['email']."'" ;

        $result = $conn->query($query_sql);
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $query_sql2="SELECT i.nome as nome, t.quantita as quantita
                          FROM inclusioneingredienti t, ingrediente i
                          WHERE t.codProdPers=".$row["id"]." AND t.codIngr = i.id AND t.tipoIngr = i.codTipo" ;
            $result2 = $conn->query($query_sql2);
            $Ingr="Ingredienti:";
            $i=0;
            while($row2 = $result2->fetch_assoc()) {
              $i=$i+1;
              $Ingr=$Ingr." ".$row2["nome"];
              if ($row2['quantita']>1) {
                $Ingr=$Ingr."x".$row2["quantita"];
              }
              if ($i<$result2->num_rows) {
                $Ingr=$Ingr.",";
              }
            }
            $myFood = new Item($row["id"],$row["nome"]." pers.",$row["prezzo"],"",1,$Ingr,"","prodPers");
            $ItemInCart[]=$myFood;
          }
        }

        $query_sql="SELECT s.nome as nome, i.quantita as quantita, s.id as id,s.prezzo as prezzo, s.sconto as sconto, p.nome as porzione
                      FROM inclusionesnack i, snack s, porzione p
                      WHERE i.codcarrello = ".$CartId." AND i.codsnack = s.id AND  p.id = s.codPorzione AND i.utente='".$_SESSION['email']."'";

        $result = $conn->query($query_sql);
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $myFood = new Item($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],"",$row["porzione"],"snack");
            $ItemInCart[]=$myFood;
          }
        }

        $query_sql="SELECT b.nome as nome, i.quantita as quantita, b.id as id,b.prezzo as prezzo, b.sconto as sconto, p.nome as porzione
                       FROM inclusionebevanda i, bevanda b, porzione p
                       WHERE i.codcarrello = ".$CartId." AND i.codbevanda = b.id AND  p.id = b.codPorzione AND i.utente='".$_SESSION['email']."'";

        $result = $conn->query($query_sql);
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $myFood = new Item($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],"",$row["porzione"],"bevanda");
            $ItemInCart[]=$myFood;
          }
        }
        $cart = new Cart($CartId,$spesatemp,$datatemp, $ItemInCart);
        $Carrelli[]=$cart;
      }
    }
  } else {
    header("Location: index.html");
  }
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ultimi Acquisti</title>

    <!-- Bootstrap CSS -->
    <?php require 'utils/commons.html'; ?>

    <link rel="stylesheet" href="../css/acquisti.css">
    <script src="../script/acquisti.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
  </head>
  <body>
    <?php require 'navbar.php' ?>
    <section class="page">
      <h1>Ultimi Acquisti</h1>
      <ol>
        <?php foreach ($Carrelli as $cart) { ?>
          <li id="<?php echo "cart".$cart->id ?>">
            <section class="row" id="<?php echo "cart".$cart->id ?>">
              <h2 class="col-7"><?php echo date_format(date_create($cart->data), 'd/m/Y'); ?></h2>
              <h2 class="col-3 prezzo"><?php echo number_format($cart->spesa,2) ?>€</h2>
              <i class="col-2 fa fa-plus" style="font-size: 25px;"></i>
            </section>
            <ul>
              <?php foreach ($cart->item as $value) { ?>
                <li class="row">
                  <div class="col-9">
                    <h3><?php echo $value->nome; ?> <?php if (isset($value->porzione)) { echo $value->porzione; } ?> <?php if ($value->quantita>1) { echo "x".$value->quantita; } ?></h3>
                    <p><?php echo $value->descrizione ?></p>
                  </div>
                  <p class="col-3"><?php echo number_format($value->prezzo * $value->quantita,2) ?>€</p>
                </li>
              <?php }?>
            </ul>
          </li>
        <?php } ?>
      </ol>
    </section>
  </body>
