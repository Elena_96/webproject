<?php
  include 'utils/functions.php';
  include 'utils/db_connect.php';
  include 'func_page/aggiungiPromo_func.php';
  $mysqli = connectToDatabase();

?>

<!DOCTYPE html>
<html>
  <head>
    <script>
    var conto;

    // return a new element to add to the form; every name will be number_name
    function getElement(number) {
      return "<div class='form-row'>\
                <div class='form-group col-6'>\
                  <span>\
                    Scegli il prodotto a cui vuoi aggiungere o modificare lo sconto\
                  </span> <br/>" +
                  <?php  $result = getAllProducts($mysqli); ?>
                 "<label for='" + number + "_menu'>\
                    Menu\
                  </label> <br/>\
                  <select class='custom-select' id='" + number + "_menu' name='" + number + "_menu[]' multiple>" +
                  <?php
                    $menu = $result[0];
                    foreach ($menu as $id => $text) {
                   ?>
                          "<option value=" + <?php echo '"' . $id . '"'; ?> + ">" +
                            <?php echo '"' . $text . '"';?> +
                          "</option>" +
                  <?php
                    }
                   ?>
                  "</select> <br/>\
                  <label for='" + number + "_prod'>\
                     Prodotti\
                   </label> <br/>\
                   <select class='custom-select' id='" + number + "_prod' name='" + number + "_prod[]' multiple>" +
                   <?php
                     $prod = $result[1];
                     foreach ($prod as $id => $text) {
                    ?>
                           "<option value=" + <?php echo '"' . $id . '"'; ?> + ">" +
                             <?php echo '"' . $text . '"';?> +
                           "</option>" +
                   <?php
                     }
                    ?>
                   "</select> <br/>\
                   <label for='" + number + "_ing'>\
                      Ingredienti\
                    </label> <br/>\
                    <select class='custom-select' id='" + number + "_ing' name='" + number + "_ing[]' multiple>" +
                    <?php
                      $ing = $result[2];
                      foreach ($ing as $id => $text) {
                     ?>
                            "<option value=" + <?php echo '"' . $id . '"'; ?> + ">" +
                              <?php echo '"' . $text . '"';?> +
                            "</option>" +
                    <?php
                      }
                     ?>
                    "</select> <br/>\
                    <label for='" + number + "_snack'>\
                       Snack\
                     </label> <br/>\
                     <select class='custom-select' id='" + number + "_snack' name='" + number + "_snack[]' multiple>" +
                     <?php
                       $prod = $result[3];
                       foreach ($prod as $id => $text) {
                      ?>
                             "<option value=" + <?php echo '"' . $id . '"'; ?> + ">" +
                               <?php echo '"' . $text . '"';?> +
                             "</option>" +
                     <?php
                       }
                      ?>
                     "</select> <br/>\
                     <label for='" + number + "_bev'>\
                        Bevande\
                      </label> <br/>\
                      <select class='custom-select' id='" + number + "_bev' name='" + number + "_bev[]' multiple>" +
                      <?php
                        $prod = $result[4];
                        foreach ($prod as $id => $text) {
                       ?>
                              "<option value=" + <?php echo '"' . $id . '"'; ?> + ">" +
                                <?php echo '"' . $text . '"';?> +
                              "</option>" +
                      <?php
                        }
                       ?>
                      "</select> <br/>\
                </div>\
                <div class='form-group col-6'>\
                   <label for='" + number +  "_sconto'>Sconto da applicare (in %)</label>\
                   <input type='number' class='form-control' value='0' min='0' max='100' step='1' id='" + number +
                   "_sconto' name='" + number + "_sconto' required>\
                </div>\
              </div>";
    }

    function addNewElement(number) {
      var fieldset = "<fieldset> <legend>Elemento " + number + "</legend>";
      fieldset += getElement(number);
      fieldset += "</fieldset> <hr>";
      var count = Number($("#nav-aggiungi form input[name='count']").val()) + 1;
      if ( count === 1) {
        $("#nav-aggiungi form > input[type='hidden']:last").after(fieldset);
      } else {
        $("#nav-aggiungi form > fieldset:last + hr").after(fieldset);
      }
      $("#nav-aggiungi form input[name='count']").val(count);
    }

    function aggiungiElemento() {
      conto++;
      addNewElement(conto);
    }

    $(document).ready(function(){
      conto = 0;
      aggiungiElemento();
      //script per rendere attivi i tooltip
      $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
  </head>
  <body>
    <div class="row">
      <div class="col-11">
        <nav>
          <div class="nav nav-tabs" id="aggiungi-rimuovi-tab" role="tablist">
            <a class="nav-item nav-link active" id="aggiungi-tab" data-toggle="tab" href="#nav-aggiungi" role="tab" aria-controls="nav-aggiungi" aria-selected="true">
              Inserisci/Modifica
            </a>
            <a class="nav-item nav-link" id="rimuovi-tab" data-toggle="tab" href="#nav-rimuovi" role="tab" aria-controls="nav-rimuovi" aria-selected="false">
              Rimuovi
            </a>
          </div>
        </nav>
        <div class="tab-content border border-top-0" id="aggiungi-rimuovi-content">
          <div class="tab-pane fade show active" id="nav-aggiungi" role="tabpanel" aria-labelledby="aggiungi-tab">
            <form action="fornitore.php" method="post" enctype="multipart/form-data">
              <!-- indica il tipo di operazione che si sta facendo -->
              <input type="hidden" name="operation" value="ins_promo">
              <!-- indica il numero di elementi presenti -->
              <input type="hidden" name="count" value="0">

              <div class="form-row justify-content-end">
                <em class="fa fa-plus-circle aggiungi-elemento" data-toggle="tooltip" data-placement="top"
                 title="Clicca per inserire un ulteriore elemento" onclick="aggiungiElemento()"></em>
                <input type="submit" class="btn btn-secondary" value="Aggiorna sconti">
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="nav-rimuovi" role="tabpanel" aria-labelledby="rimuovi-tab">
            <form action="fornitore.php" method="post">
              <div class="form-row">
                <!-- indica il tipo di operazione che si sta facendo -->
                <input type="hidden" name="operation" value="del_promo">
                <div class="form-group col">
                  <label for="selezione">Seleziona le offerte che vuoi eliminare</label> <br/>
                  <select class="custom-select" id="selezione" name="selezione[]" multiple required>
                    <?php
                      $result = getAllSales($mysqli);
                      foreach ($result as $id => $text) {
                     ?>
                            <option value = <?php echo '"' . $id . '"'; ?>>
                              <?php echo $text;?>
                            </option>
                    <?php
                      }
                     ?>
                  </select>
                </div>
              </div>
              <div class="form-row justify-content-end">
                <input type="submit" class="btn btn-secondary" value="Rimuovi sconto/i">
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-1"></div>
    </div>
  </body>
</html>
