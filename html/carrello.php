<div class="collapse navbar-collapse" id="carrello">
  <section>
    <img src="../img/cart.png" alt="Carrello">
    <h1>Carrello <?php if (empty($ItemInCart)) {echo "vuoto";} ?></h1>
  </section>
  <section>
    <?php if ($login && !empty($ItemInCart)) { ?>
      <section class="row">
        <h2 class="col-8 tot">Spesa totale: <?php $tot=0; foreach ($ItemInCart as $value) {$tot=$tot+$value->prezzo*$value->quantita;}; echo number_format($tot,2) ?>€</h2>
        <a href="pagamento.php" class="col-4 button">Acquista</a>
      </section>
      <ul>
        <?php foreach ($ItemInCart as $value) { ?>
          <li class="row" id="<?php echo $value->id ?>">
            <div class="col-8">
              <h2><?php echo $value->nome; ?> <?php if (isset($value->porzione)) { echo $value->porzione; } ?> <?php if ($value->quantita>1) { echo "x".$value->quantita; } ?></h2>
              <p><?php echo $value->descrizione ?></p>
            </div>
            <p class="col-2" style="text-align: center;"><?php echo number_format($value->prezzo * $value->quantita,2) ?>€</p>
            <i class="material-icons col-2" style="text-align: center;" id="<?php echo $value->id ?>">cancel</i>
          </li>
        <?php } ?>
      </ul>
  <?php } else if (!$login) { ?>
    <a href="logIn.php" class="button">Eseguire il login per poter inserire le nostre prelibatezze nel carrello.</a>
  <?php } ?>
  </section>
</div>
