<?php
if (isset($_GET['q'])) {
  $pagina = $_GET['q'];
  switch ($_GET['q']) {
    case 'prodottoStandard':
    case 'ingrediente':
    case 'menu':
    case 'bevanda':
    case 'snack':

    include 'utils/functions.php';
    include 'utils/db_connect.php';
    include 'func_page/aggiungiElemento_func.php';
    $mysqli = connectToDatabase();

?>

<!DOCTYPE html>
<html>
  <head>
    <script>
    var conto;

    // return a new element to add to the form; every name will be number_name
    function getElement(number) {
      return "<div class='form-row'>\
              <div class='form-group col-6'>\
                <label for='" + number + "_nome'>\
                  Nome " + <?php echo '"' .  getInsertName($pagina) . '"'; ?> +
                "</label>\
                 <input type='text' class='form-control' id='" + number + "_nome' name='"+ number +
                 "_nome' placeholder='Inserisci il nome " + <?php  echo '"' .  getInsertName($pagina) . '"';?> + "' required>\
                 <label for='" + number +  "_prezzo'>Prezzo</label>\
                 <input type='number' class='form-control' value='0.00' min='0' step='0.01' id='" + number +
                 "_prezzo' name='" + number + "_prezzo' required>\
                 <label for='" + number + "_immagine'>\
                  Scegli l'immagine " + <?php  echo '"' .  getInsertName($pagina) . '"';?> +
                "</label>\
                 <input type='file' class='form-control-file' id='" + number + "_immagine' name='" + number + "_immagine' required>\
              </div>\
              <div class='form-group col-6'>" +
        <?php
          switch($pagina) {
            case "bevanda":
            case "snack":
        ?>
                      "<label for='" + number + "_porzione'>Porzione</label> <br/>\
                       <select class='custom-select' id='" + number + "_porzione' name='" + number + "_porzione' required>" +
            <?php
                $result = getAllElementsOf($mysqli, "porzione");
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
            ?>
                            "<option value =" + <?php $id = getIdOf("porzione", $row); echo '"' . $id . '"'; ?> + ">" +
                              <?php echo '"' . $row["nome"] . '"'; ?> +
                            "</option>" +
            <?php
                    }
                } else if($mysqli->error) {
                    echo '"' . "Query reperimento dati di porzione: " . $mysqli->error . '"';
                }
            ?>
                      "</select>" +
            <?php
                break;
              case "prodottoStandard":
             ?>
                      "<label for='" + number + "_tipoProdotto'>Tipo di prodotto che si sta inserendo</label> <br/>\
                       <select class='custom-select' id='" + number + "_tipoProdotto' name='" + number + "_tipoProdotto' required>" +
             <?php
                 $result = getAllElementsOf($mysqli, "tipoProdotto");
                 if ($result->num_rows > 0) {
                     while($row = $result->fetch_assoc()) {
              ?>
                           "<option value =" + <?php $id = getIdOf("tipoProdotto", $row); echo '"' . $id . '"'; ?> + ">" +
                             <?php echo '"' . $row["nome"] . '"'; ?> +
                           "</option>" +
             <?php
                     }
                 } else if($mysqli->error) {
                     echo '"' . "Query reperimento dati di tipoProdotto: " . $mysqli->error . '"';
                 }
              ?>
                      "</select> <br/>\
                       <label for='" + number + "_descrizione'>Inserisci la descrizione del prodotto</label>\
                       <textarea class='form-control' maxlength='255' rows='3' id='" + number + "_descrizione' name='" + number +
                       "_descrizione' required></textarea>" +
              <?php
                  break;
                case "ingrediente":
              ?>
                      "<label for='" + number + "_tipoIngrediente'>Tipo di ingrediente che si sta inserendo</label> <br/>\
                       <select class='custom-select' id='" + number + "_tipoIngrediente' name='" + number + "_tipoIngrediente' required>" +
              <?php
                $result = getAllElementsOf($mysqli, "tipoIngrediente");
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
               ?>
                          "<option value=" + <?php $id = getIdOf("tipoIngrediente", $row); echo '"' . $id . '"'; ?> + ">" +
                             <?php echo '"' . $row["nome"] . '"'; ?> +
                          "</option>" +
              <?php
                    }
                } else if($mysqli->error) {
                    echo '"' + "Query reperimento dati di porzione: " . $mysqli->error + '"';
                }
               ?>
                      "</select>" +
              <?php
                  break;
                case "menu":
              ?>
                      "<label for='" + number + "_prodottoStandard'>Seleziona i prodotti inclusi nel menu</label> <br/>\
                       <select class='custom-select' id='" + number + "_prodottoStandard' name='" + number +
                       "_prodottoStandard[]' multiple required>" +
              <?php
                $result = getAllElementsOf($mysqli, "prodottoStandard");
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
               ?>
                          "<option value=" + <?php $id = getIdOf("prodottoStandard", $row); echo '"' . $id . '"'; ?> + ">" +
                             <?php echo '"' . $row["nomeProdotto"] . '"'; ?> +
                          "</option>" +
              <?php
                    }
                } else if($mysqli->error) {
                    echo '"' + "Query reperimento dati di prodottoStandard: " . $mysqli->error + '"';
                }
               ?>
                      "</select>\
                      <label for='" + number + "_bevanda' hidden>Seleziona i prodotti inclusi nel menu</label>\
                      <select class='custom-select' id='" + number + "_bevanda' name='" + number + "_bevanda[]' multiple required>" +
              <?php
                $result = getAllElementsOf($mysqli, "bevanda");
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
               ?>
                          "<option value =" + <?php $id = getIdOf("bevanda", $row); echo '"' . $id . '"'; ?> + ">" +
                              <?php echo '"' . $row["nomeProdotto"] . " " . $row["porzione"] . '"'; ?> +
                          "</option>" +
              <?php
                    }
                } else if($mysqli->error) {
                    echo '"' . "Query reperimento dati di bevanda: " . $mysqli->error . '"';
                }
               ?>
                      "</select>\
                      <label for='" + number + "_snack' hidden>Seleziona i prodotti inclusi nel menu</label>\
                      <select class='custom-select' id='" + number + "_snack' name='" + number + "_snack[]' multiple required>" +
              <?php
                $result = getAllElementsOf($mysqli, "snack");
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
               ?>
                        "<option value =" + <?php $id = getIdOf("snack", $row); echo '"' . $id . '"'; ?> + ">" +
                          <?php echo '"' . $row["nomeProdotto"] . " " . $row["porzione"] . '"'; ?> +
                        "</option>" +
              <?php
                    }
                } else if($mysqli->error) {
                    echo '"' . "Query reperimento dati di snack: " . $mysqli->error . '"';
                }
               ?>
                      "</select>" +
              <?php
                break;
                default: break;
              }
               ?>
                "</div>\
              </div>";
    }

    function addNewElement(number) {
      var fieldset = "<fieldset> <legend>Elemento " + number + "</legend>";
      fieldset += getElement(number);
      fieldset += "</fieldset> <hr>";
      var count = Number($("#nav-aggiungi form input[name='count']").val()) + 1;
      if ( count === 1) {
        $("#nav-aggiungi form > input[type='hidden']:last").after(fieldset);
      } else {
        $("#nav-aggiungi form > fieldset:last + hr").after(fieldset);
      }
      $("#nav-aggiungi form input[name='count']").val(count);
    }

    function aggiungiElemento() {
      conto++;
      addNewElement(conto);
    }

    $(document).ready(function(){
      conto = 0;
      aggiungiElemento();
      //script per rendere attivi i tooltip
      $('[data-toggle="tooltip"]').tooltip();
    });
    </script>
  </head>
  <body>
    <div class="row">
      <div class="col-11">
        <nav>
          <div class="nav nav-tabs" id="aggiungi-rimuovi-tab" role="tablist">
            <a class="nav-item nav-link active" id="aggiungi-tab" data-toggle="tab" href="#nav-aggiungi" role="tab" aria-controls="nav-aggiungi" aria-selected="true">
              Inserisci
            </a>
            <a class="nav-item nav-link" id="rimuovi-tab" data-toggle="tab" href="#nav-rimuovi" role="tab" aria-controls="nav-rimuovi" aria-selected="false">
              Rimuovi
            </a>
          </div>
        </nav>
        <div class="tab-content border border-top-0" id="aggiungi-rimuovi-content">
          <div class="tab-pane fade show active" id="nav-aggiungi" role="tabpanel" aria-labelledby="aggiungi-tab">
            <form action="fornitore.php" method="post" enctype="multipart/form-data">
              <!-- indica il tipo di operazione che si sta facendo -->
              <input type="hidden" name="operation" value="ins">
              <!-- indica cosa si vuole aggiungere -->
              <input type="hidden" name="what" value=<?php echo '"' . $pagina . '"'; ?>>
              <!-- indica il numero di elementi presenti -->
              <input type="hidden" name="count" value="0">

              <div class="form-row justify-content-end">
                <em class="fa fa-plus-circle aggiungi-elemento" data-toggle="tooltip" data-placement="top"
                 title="Clicca per inserire un ulteriore elemento" onclick="aggiungiElemento()"></em>
                <input type="submit" class="btn btn-secondary" value="Aggiungi elemento/i">
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="nav-rimuovi" role="tabpanel" aria-labelledby="rimuovi-tab">
            <form action="fornitore.php" method="post">
              <div class="form-row">
                <!-- indica il tipo di operazione che si sta facendo -->
                <input type="hidden" name="operation" value="del">
                <!-- indica cosa si vuole eliminare -->
                <input type="hidden" name="what" value=<?php echo '"' . $pagina . '"'; ?>>
                <div class="form-group col">
                  <label for="selezione">Seleziona <?php echo getSelectText($pagina);?> che vuoi eliminare</label> <br/>
                  <select class="custom-select" id="selezione" name="selezione[]" multiple required>
                    <?php
                      $result = getAllElementsOf($mysqli, $pagina);
                      if ($result->num_rows > 0) {
                          while($row = $result->fetch_assoc()) {
                     ?>
                            <option value = <?php $id = getIdOf($pagina, $row); echo '"' . $id . '"'; ?>><?php
                            echo $row["nomeProdotto"];
                            if($pagina == "bevanda" || $pagina == "snack"){
                              echo " " . $row["porzione"];
                            } ?></option>
                    <?php
                          }
                      } else if($mysqli->error) {
                          echo "Query reperimento dati di " . $pagina . ": " . $mysqli->error;
                      }
                     ?>
                  </select>
                </div>
              </div>
              <div class="form-row justify-content-end">
                <input type="submit" class="btn btn-secondary" value="Rimuovi elemento/i">
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-1"></div>
    </div>
  </body>
</html>
<?php
  break;
  default:
    echo "errore -> " . $_GET["q"];
    break;
  }
} ?>
