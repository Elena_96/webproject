<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <?php
      require '../utils/db_connect.php';
      $mysqli = connectToDatabase();
      $sql = "SELECT COUNT(id) AS 'conto' FROM notifica WHERE destinatario = 'adminSupremo' AND visto = 0";
      $res = $mysqli->query($sql);
      if($res->num_rows == 1) {
          $row = $res->fetch_assoc();
          if($row["conto"] > 0) {
    ?>
    <span class="badge badge-info"> <?php echo $row["conto"]; ?> </span>
    <?php
          }
      }
    ?>
  </body>
</html>
