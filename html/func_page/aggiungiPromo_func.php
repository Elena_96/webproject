<?php

function getAllSales($mysqli) {
  $returnArray = array();
  // ---------------- menu ----------------
  $sql = "SELECT id, nome, sconto
          FROM menu
          WHERE sconto IS NOT NULL";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $returnArray["menu_" . $row["id"]] = $row["sconto"] . "% su " . $row["nome"];
    }
  }
  // ---------------- prodotti ----------------
  $sql = "SELECT id, nome, sconto
          FROM prodottoStandard
          WHERE sconto IS NOT NULL";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $returnArray["prod_" . $row["id"]] = $row["sconto"] . "% su " . $row["nome"];
    }
  }
  // ---------------- ingredienti ----------------
  $sql = "SELECT id, nome, codTipo, sconto
          FROM ingrediente
          WHERE sconto IS NOT NULL";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $returnArray["ing_" . $row["id"] . "_" . $row["codTipo"]] = $row["sconto"] . "% su " . $row["nome"];
    }
  }
  // ---------------- snack ----------------
  $sql = "SELECT s.id, s.nome AS 'nomeSnack', p.nome AS 'porzione', s.sconto
          FROM snack s, porzione p
          WHERE p.id = s.codPorzione AND s.sconto IS NOT NULL";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $returnArray["snck_" . $row["id"]] = $row["sconto"] . "% su " . $row["nomeSnack"] . " " . $row["porzione"];
    }
  }
  // ---------------- bevande ----------------
  $sql = "SELECT b.id, b.nome AS 'nomeBevanda', p.nome AS 'porzione', b.sconto
          FROM bevanda b, porzione p
          WHERE p.id = b.codPorzione AND b.sconto IS NOT NULL";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $returnArray["bev_" . $row["id"]] = $row["sconto"] . "% su " . $row["nomeBevanda"] . " " . $row["porzione"];
    }
  }
  return $returnArray;
}

function getAllProducts($mysqli) {
  $returnArray = array();
  $menuArr = array();
  $prodArr = array();
  $ingArr = array();
  $snackArr = array();
  $bevArr = array();
  // ---------------- menu ----------------
  $sql = "SELECT id, nome, sconto
          FROM menu";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $msg = $row["nome"];
      if ($row["sconto"] !== null) {
        $msg .= " (" . $row["sconto"] . "% già presente)";
      }
      $menuArr["menu_" . $row["id"]] = $msg;
    }
  }
  // ---------------- prodotti ----------------
  $sql = "SELECT id, nome, sconto
          FROM prodottoStandard";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $msg = $row["nome"];
      if ($row["sconto"] !== null) {
        $msg .= " (" . $row["sconto"] . "% già presente)";
      }
      $prodArr["prod_" . $row["id"]] = $msg;
    }
  }
  // ---------------- ingrediente ----------------
  $sql = "SELECT id, codTipo, nome, sconto
          FROM ingrediente
          ORDER BY codTipo";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $msg = $row["nome"];
      if ($row["sconto"] !== null) {
        $msg .= " (" . $row["sconto"] . "% già presente)";
      }
      $ingArr["ing_" . $row["id"] . "_" . $row["codTipo"]] = $msg;
    }
  }
  // ---------------- snack ----------------
  $sql = "SELECT s.id, s.nome AS 'nomeSnack', p.nome AS 'porzione', s.sconto
          FROM snack s, porzione p
          WHERE p.id = s.codPorzione";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $msg = $row["nomeSnack"] . " " . $row["porzione"];
      if ($row["sconto"] !== null) {
        $msg .= " (" . $row["sconto"] . "% già presente)";
      }
      $snackArr["snck_" . $row["id"]]= $msg;
    }
  }
  // ---------------- bevande ----------------
  $sql = "SELECT b.id, b.nome AS 'nomeBevanda', p.nome AS 'porzione', b.sconto
          FROM bevanda b, porzione p
          WHERE p.id = b.codPorzione";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $msg = $row["nomeBevanda"] . " " . $row["porzione"];
      if ($row["sconto"] !== null) {
        $msg .= " (" . $row["sconto"] . "% già presente)";
      }
      $bevArr["bev_" . $row["id"]] = $msg;
    }
  }
  array_push($returnArray,$menuArr, $prodArr, $ingArr, $snackArr, $bevArr);
  return $returnArray;
}

 ?>
