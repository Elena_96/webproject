<?php

  function getAllElementsOf($mysqli, $tabellaInclusione, $tabella, $codProd, $user, $codCarr) {
    $sql = "SELECT f.nome AS 'nome', i.quantita ";
    if($tabella == "bevanda" || $tabella == "snack") {
      $sql .= ", p.nome AS 'porzione' ";
    }
    $sql .= "FROM " . $tabellaInclusione . " i, " . $tabella . " f ";
    if($tabella == "bevanda" || $tabella == "snack") {
      $sql .= ", porzione p ";
    }
    $sql .= "WHERE i.codCarrello = " . $codCarr .
            " AND i.utente = '" . $user . "'" .
            " AND f.id = i." . $codProd;
    if($tabella == "bevanda" || $tabella == "snack") {
      $sql .= " AND p.id = f.codPorzione";
    }
    $returnArray = array();
    $res = $mysqli->query($sql);
    if($res->num_rows > 0) {
      while ($row = $res->fetch_assoc()) {
        $msg = "x" . $row["quantita"] . " - ";
        switch ($tabella) {
          case 'menu':
            $msg .= "Menu " . $row["nome"];
            break;
          case 'prodottoStandard':
            $msg .= $row["nome"];
            break;
          case 'bevanda':
          case 'snack':
            $msg .= $row["nome"] . " | Porzione " . $row["porzione"];
            break;
          default:
            $msg .= "";
            break;
        }
        array_push($returnArray, $msg);
      }
    } else if($mysqli->error){
      echo $mysqli->error . "<br/>";
    }
    return $returnArray;
  }

  function getAllPaniniPers($mysqli, $user, $codCarr) {
    $sql = "SELECT i.nome, c.quantita, p.id AS 'paninoId'
            FROM inclusioneIngredienti c, prodottoPersonalizzato p, ingrediente i
            WHERE p.utente = '" . $user . "'" .
          " AND p.codCarrello = " . $codCarr .
          " AND p.id = c.codProdPers
            AND c.codIngr = i.id
            AND c.tipoIngr = i.codTipo
            ORDER BY p.id, i.codTipo";
    $returnArray = array();
    $res = $mysqli->query($sql);
    if($res->num_rows > 0) {
      $id = -1;
      $tmp = array();
      while ($row = $res->fetch_assoc()) {
        if($id > 0 && $id != $row["paninoId"]) {
          array_push($returnArray, $tmp);
          $tmp = array();
        }
        $tmp[$row["nome"]] = $row["quantita"];
        $id = $row["paninoId"];
      }
      array_push($returnArray, $tmp);
    } else if($mysqli->error){
      echo $mysqli->error . "<br/>";
    }
    return $returnArray;
  }


?>
