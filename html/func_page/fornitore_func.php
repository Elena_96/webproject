<?php

function addElements($data, $mysqli) {
  $x = 1;
  $elem = $data["what"];
  $count = $data["count"];
  $stmt = "INSERT INTO " . $elem . " (nome, img, prezzo";
  switch($elem) {
    case 'prodottoStandard':
      $stmt .= ", descrizione, codTipo";
      break;
    case 'bevanda':
    case 'snack':
      $stmt .= ", codPorzione";
      break;
    case 'ingrediente':
      $stmt .= ", codTipo, id";
      break;
    case "menu":
    default:
      break;
  }
  $stmt .= ") VALUES (";
  $oldCodTipo = -1;
  $conteggio;
  while($x <= $count) {
    $stmt .= '"' . $data["" . $x . "_nome"] . '",';
    $imgPath = getImgPath($x, $elem);
    $stmt .= (empty($imgPath) ? "NULL" : ('"' . $imgPath . '"')) . ",";
    $stmt .= $data["" . $x . "_prezzo"];
    switch($elem) {
      case 'prodottoStandard':
        $stmt .= ',"' . $data["" . $x . "_descrizione"] . '",';
        $stmt .= $data["" . $x . "_tipoProdotto"];
        break;
      case 'bevanda':
      case 'snack':
        $stmt .= "," . $data["" . $x . "_porzione"];
        break;
      case 'ingrediente':
        $codTipo = $data["" . $x . "_tipoIngrediente"];
        $stmt .= "," . $codTipo . ",";
        if($codTipo === $oldCodTipo) {
          $conteggio++;
        } else {
          $conteggio = 1;
          $oldCodTipo = $codTipo;
        }
        $idIngr = getLastIngrId($mysqli, $codTipo) + $conteggio;
        $stmt .= $idIngr != -1 ? $idIngr : 99;
        break;
      case "menu":
      default:
        break;
    }
    $stmt .= ")" . ($x == $count ? "" : ", (");
    $x++;
  }
  $ok = $mysqli->query($stmt);
  if ($ok && $elem == "menu") {
      $ok = insertMenuComponents($mysqli, $count, $data);
  }
  if ($ok) {
    alert("Inserimento di " . $count . " elementi/o avvenuta con successo");
  } else {
    alert("Errore nell'inserimento: " . $mysqli->error);
  }
}

function deleteElements($data, $mysqli) {
  $elem = $data["what"];
  $stmt = "DELETE FROM " . $elem . " WHERE ";
  switch($elem) {
    case 'prodottoStandard':
    case 'menu':
    case 'bevanda':
    case 'snack':
      $stmt .= "id IN (";
      $array = $data["selezione"];
      $count = is_array($array) ? count($array) : 1;
      for ($i = 0; $i < $count ; $i++) {
        $stmt .= $array[$i];
        if($i < $count - 1) {
          $stmt .= ", ";
        }
      }
      $stmt .= ")";
      break;
    case 'ingrediente':
      $array = $data["selezione"];
      $count = is_array($array) ? count($array) : 1;
      for ($i = 0; $i < $count ; $i++) {
        list($id, $tipo) = explode("_", $array[$i]);
        $stmt .= "(id = " . $id . " AND codTipo = " . $tipo . ")";
        if($i < $count - 1) {
          $stmt .= " OR ";
        }
      }
      break;
    default:
      echo "error with parameter; Received " . $elem . "<br/>";
      break;
  }
  if ($mysqli->query($stmt) === TRUE) {
    alert("Eliminazione di " . $count . " elementi/o avvenuta con successo");
  } else {
    alert("Errore nell'eliminazione: " . $mysqli->error);
  }
}

  function sendNotification($op, $data, $mysqli) {
    $msg;
    if($op == "prep") {
      $msg = "Il tuo ordine è in preparazione! Ti avviseremo quando lo spediremo";
    } else {
      $msg = "Il tuo ordine è finalmente pronto!!! Preparati a riceverlo a momenti";
    }
    $sql = "INSERT INTO notifica (destinatario, messaggio, visto) VALUES ";
    $sql .= '("' . $data["destinatario"] . '", "' . $msg . '", 0)';
    $ok = $mysqli->query($sql);
    if ($ok) {
        if ($op == "prep") {
          $sql = "UPDATE notifica SET messaggio = 'in preparazione' WHERE id = " . $data["notificaAdmin"];
        } else {
          $sql = "UPDATE notifica SET visto = 1 WHERE id = " . $data["notificaAdmin"];
        }
        $ok = $mysqli->query($sql);
    }
    if($ok) {
      alert("Cambio di stato avvenuto! Notifica inviata");
    } else {
      alert("Errore nella notificazione: " . $mysqli->error);
    }
  }

function removePromo($data, $mysqli) {
  if (updatePromo($data["selezione"], $mysqli, "NULL")) {
    alert("Sconti rimossi con successo");
  } else {
    alert("Errore nell'eliminazione: " . $mysqli->error);
  }
}

function addPromo($data, $mysqli) {
  $x = 1;
  $num_elem = $data["count"];
  $array = array();
  while($x <= $num_elem) {
    $count = isset($data[$x . "_menu"]) ? count($data[$x . "_menu"]) : 0;
    for($i = 0; $i < $count; $i++) {
      array_push($array, $data[$x . "_menu"][$i]);
    }
    $count = isset($data[$x . "_prod"]) ? count($data[$x . "_prod"]) : 0;
    for($i = 0; $i < $count; $i++) {
      array_push($array, $data[$x . "_prod"][$i]);
    }
    $count = isset($data[$x . "_ing"]) ? count($data[$x . "_ing"]) : 0;
    for($i = 0; $i < $count; $i++) {
      array_push($array, $data[$x . "_ing"][$i]);
    }
    $count = isset($data[$x . "_snack"]) ? count($data[$x . "_snack"]) : 0;
    for($i = 0; $i < $count; $i++) {
      array_push($array, $data[$x . "_snack"][$i]);
    }
    $count = isset($data[$x . "_bev"]) ? count($data[$x . "_bev"]) : 0;
    for($i = 0; $i < $count; $i++) {
      array_push($array, $data[$x . "_bev"][$i]);
    }
    if (!updatePromo($array, $mysqli, $data[$x . "_sconto"])) {
      alert("Errore nell'aggiornamento: " . $mysqli->error);
      return;
    }
    $x++;
    $array = array();
  }
  alert("Sconti aggiornati con successo");
}

function updatePromo($array, $mysqli, $value) {
  if($value === "0") {
    $value = "NULL";
  }
  $menuUp = "UPDATE menu SET sconto = " . $value . " WHERE id IN ";
  $menuUp .= "(";
  $prodUp = "UPDATE prodottoStandard SET sconto = " . $value . " WHERE id IN ";
  $prodUp .= "(";
  $bevUp = "UPDATE bevanda SET sconto = " . $value . " WHERE id IN ";
  $bevUp .= "(";
  $snackUp = "UPDATE snack SET sconto = " . $value . " WHERE id IN ";
  $snackUp .= "(";
  $ingUp = "UPDATE ingrediente SET sconto = " . $value . " WHERE ";
  $menuOk = $prodOk = $bevOk = $snackOk = $ingOk = false;

  //$array = $data["selezione"];
  $count = is_array($array) ? count($array) : 1;
  for ($i = 0; $i < $count ; $i++) {
    list($tipo, $id) = explode("_", $array[$i]);
    $codTipo;
    if($tipo == "ing") {
      list($tipo, $id, $codTipo) = explode("_", $array[$i]);
    }
    switch($tipo) {
      case "menu":
        if($menuOk) {
          $menuUp .= " ,";
        } else {
          $menuOk = true;
        }
        $menuUp .= " " . $id;
        break;
      case "prod":
        if($prodOk) {
          $prodUp .= " ,";
        } else {
          $prodOk = true;
        }
        $prodUp .= " " . $id;
        break;
      case "snck":
        if($snackOk) {
          $snackUp .= " ,";
        } else {
          $snackOk = true;
        }
        $snackUp .= " " . $id;
        break;
      case "bev":
        if($bevOk) {
          $bevUp .= " ,";
        } else {
          $bevOk = true;
        }
        $bevUp .= " " . $id;
        break;
      case "ing":
        if($ingOk) {
          $ingUp .= " OR ";
        } else {
          $ingOk = true;
        }
        $ingUp .= "(id = " . $id . " AND codTipo = " . $codTipo . ")";
        break;
      default: break;
    }
  }
  $menuUp .= ")";
  $bevUp .= ")";
  $snackUp .= ")";
  $prodUp .= ")";
  //echo $menuUp . "<br/>" . $bevUp . "<br/>" . $snackUp . "<br/>" . $prodUp . "<br/>" . $ingUp. "<br/>" ;
  if($menuOk) {
    if(!$mysqli->query($menuUp)) {
      return false;
    }
  }
  if($prodOk) {
    if(!$mysqli->query($prodUp)){
      return false;
    }
  }
  if($bevOk) {
    if(!$mysqli->query($bevUp)) {
      return false;
    }
  }
  if($snackOk) {
    if(!$mysqli->query($snackUp)) {
      return false;
    }
  }
  if($ingOk) {
    if(!$mysqli->query($ingUp)) {
      return false;
    }
  }
  return true;
}

function getImgPath($number, $pagina) {
  $file = "" . $number . "_immagine";
  $target_dir = "../img/" . ($pagina == "ingrediente" ? "immagini/" : "");
  $target_file = $target_dir . basename($_FILES[$file]["name"]);
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  // Check if image file is a actual image or fake image
  if(isset($_POST["submit"])) {
      $check = getimagesize($_FILES[$file]["tmp_name"]);
      if(!$check){
          alert("Il file caricato non era un'immagine");
          return "";
      }
  }
  // Allow certain file formats
  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
      alert("Formato dell'immagine non supportato; si accettano solo JPG, JPEG e PNG. L'immagine non verrà salvata");
      return "";
  }
  if (move_uploaded_file($_FILES[$file]["tmp_name"], $target_file)) {
      return basename( $_FILES[$file]["name"]);
  } else {
      return "";
  }
}

function getLastIngrId($mysqli, $codTipo) {
  $res = $mysqli->prepare("SELECT MAX(id) FROM ingrediente WHERE codTipo = ?");
  $res->bind_param('i', $codTipo);
  $res->execute();
  $res->store_result();
  $res->bind_result($ingrId);
  $res->fetch();
  if ($res->num_rows == 1) {
    return $ingrId;
  } else {
    return -1;
  }
}

function insertMenuComponents($mysqli, $count, $data) {
  $bevStmt = "INSERT INTO presenzaBevanda (codMenu, codBevanda) VALUES ";
  $prodStmt = "INSERT INTO presenzaProdStand (codMenu, codProdStand) VALUES ";
  $snackStmt = "INSERT INTO presenzaSnack (codMenu, codSnack) VALUES ";
  $snack = false;
  $arrayMenuId = getInsertedMenuId($mysqli, $count);
  $arrLenght = count($arrayMenuId);
  for ($i=0; $i < $arrLenght; $i++) {
    $bevStmt .= "(";
    $snackStmt .= "(";
    $prodStmt .= "(";
    $array = $data["" . ($i + 1) . "_bevanda"];
    $count = is_array($array) ? count($array) : 1;
    for ($j = 0; $j < $count ; $j++) {
      $bevStmt .= $arrayMenuId[$i] . ", " . $array[$j] . ")";
      if($j < $count - 1) {
        $bevStmt .= ", (";
      }
    }
    $array = $data["" . ($i + 1) . "_prodottoStandard"];
    $count = is_array($array) ? count($array) : 1;
    for ($j = 0; $j < $count ; $j++) {
      $prodStmt .= $arrayMenuId[$i] . ", " . $array[$j] . ")";
      if($j < $count - 1) {
        $prodStmt .= ", (";
      }
    }
    $array = $data["" . ($i + 1) . "_snack"];
    $count = is_array($array) ? count($array) : 1;
    for ($j = 0; $j < $count ; $j++) {
      $snackStmt .= $arrayMenuId[$i] . ", " . $array[$j] . ")";
      if($j < $count - 1) {
        $snackStmt .= ", (";
      }
    }
    if($i < $arrLenght - 1){
      $bevStmt .= ", ";
      $prodStmt .= ", ";
      $snackStmt .= ", ";
    }
  }
  $sql = $bevStmt . "; " . $prodStmt . "; " . $snackStmt;
  return mysqli_multi_query($mysqli, $sql);
}

function getInsertedMenuId($mysqli, $count) {
  $arr = array();
  $res = $mysqli->query("SELECT id FROM menu ORDER BY id DESC LIMIT " . $count);
  if ($res->num_rows > 0) {
    while($row = $res->fetch_assoc()) {
      array_push($arr,$row["id"]);
    }
    return array_reverse($arr);
  } else {
    return $arr;
  }
}



 ?>
