<?php

/* Returns an array with the current data:
 * 0 => the number of users
 * 1 => the average age
 * 2 => the number of men
 * 3 => the number of women
 * 4 => an array with infos about the range of the users' age
 */
function getUserData($mysqli) {
  $returnArray = array();
  $sql = "SELECT sesso, dataNascita
          FROM utente
          WHERE fornitore = 0";
  $result = $mysqli->query($sql);
  $userCount = 0;
  $avgAge = 0;
  if ($result->num_rows > 0) {
      $userCount = $result->num_rows;
      $m = 0; $f = 0;
      $age = array('15' => 0, '20' => 0,
                   '25' => 0, '30' => 0,
                   '40' => 0, '45' => 0);
      $keys = array_keys($age);
      $keys_length = count($keys);
      while($row = $result->fetch_assoc()) {
          if($row["sesso"] == 'M') {
            $m++;
          } else {
            $f++;
          }
          $userAge = strtotime($row["dataNascita"]);
          $addAge = date("Y") - date("Y",$userAge);
          $avgAge += $addAge;
          // considering all the age limit values
          for ($i=0; $i < $keys_length; $i++) {
            $limit = intval($keys[$i]);
            // if the age it's higher than the current limit
            if ($addAge >= $limit) {
              // and lower than the next, only if it's not the last value, I can add it to the array
              if ($i < $keys_length - 1 && $addAge < intval($keys[$i + 1])) {
                $age[strval($limit)]++;
                break;
              } else if ($i == $keys_length - 1) {
                $age[strval($limit)]++;
                break;
              }
            }
          }
      }
      $avgAge /= $userCount;
      array_push($returnArray, $userCount, $avgAge, $m, $f, $age);
  } else if($mysqli->error) {
      echo "Query statistiche utente: " . $mysqli->error;
  }
  return $returnArray;
}

/* Returns an array with the current data:
 * 0 => the number of carts
 * 1 => the sum of every spending
 * 2 => the max spending
 * 3 => the user that did the max spending
 * 4 => how many spendings in the current month
 */
function getCartData($mysqli) {
  $sql = "SELECT c.prezzototale AS 'spesa', c.dataacquisto as 'data', u.nome, u.cognome
          FROM carrello c, utente u
          WHERE c.dataacquisto IS NOT NULL
          AND c.utente = u.user";
  $cartCount = 0;
  $maxCart = 0;
  $lastBuy = 0;
  $sumCart = 0;
  $user = "";
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    $cartCount = $result->num_rows;
    while($row = $result->fetch_assoc()) {
      $spesa = $row["spesa"];
      $data = strtotime($row["data"]);
      $sumCart += $spesa;
      if ($spesa > $maxCart) {
        $maxCart = $spesa;
        $user = $row["nome"] . " " . $row["cognome"];
      }
      if(date("mY") == date("mY",$data)) {
        $lastBuy++;
      }
    }
  } else if($mysqli->error) {
      echo "Query statistiche carrello: " . $mysqli->error;
  }
  $returnArray = array($cartCount, $sumCart, $maxCart, $user, $lastBuy);
  return $returnArray;
}

function getFoodData($mysqli, $max, $tableFood, $tableInclusione, $inclusioneFoodName) {
  // the order by is decreasing, in this way I can get the higher values
  $sql = "SELECT SUM(i.quantita) AS 'somma', f.nome
          FROM " . $tableInclusione . " i, " . $tableFood . " f, carrello c
          WHERE c.dataacquisto IS NOT NULL
          AND i.utente = c.utente
          AND i.codcarrello = c.id
          AND i." . $inclusioneFoodName . " = f.id
          GROUP BY f.id
          ORDER BY SUM(i.quantita) DESC
          LIMIT " . $max;
  $returnArray = array();
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $returnArray[$row["nome"]] = $row["somma"];
    }
  } else if($mysqli->error) {
      echo "Query statistiche " . $tableFood . ": " . $mysqli->error;
  }
  return $returnArray;
}

function getFoodPortionData($mysqli, $max, $tableFood, $tableInclusione, $inclusioneFoodName) {
  // the order by is decreasing, in this way I can get the higher values
  $sql = "SELECT SUM(i.quantita) AS 'somma', f.nome AS 'prodotto', p.nome AS 'quantita'
          FROM " . $tableInclusione . " i, " . $tableFood . " f, carrello c, porzione p
          WHERE c.dataacquisto IS NOT NULL
          AND i.utente = c.utente
          AND i.codcarrello = c.id
          AND i." . $inclusioneFoodName . " = f.id
          AND f.codPorzione = p.id
          GROUP BY f.id, p.id
          ORDER BY SUM(i.quantita) DESC
          LIMIT " . $max;
  $returnArray = array();
  $result = $mysqli->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $returnArray[$row["prodotto"] . " " . $row["quantita"]] = $row["somma"];
    }
  } else if($mysqli->error) {
      echo "Query statistiche " . $tableFood . ": " . $mysqli->error;
  }
  return $returnArray;
}

/* Returns an array with the keys corrisponding to the menu name
 * and the value corresponding to its sale numbers
 */
function getMenuData($mysqli, $maxMenu) {
  return getFoodData($mysqli, $maxMenu, "menu", "inclusioneMenu", "codMenu");
}

/* Returns an array with the keys corrisponding to the burger name
 * and the value corresponding to its sale numbers
 */
function getBurgerData($mysqli, $maxBurger) {
  return getFoodData($mysqli, $maxBurger, "prodottoStandard", "inclusioneProdStand", "codProdStand");
}

/* Returns an array with the keys corrisponding to the snack name
 * and the value corresponding to its sale numbers
 */
function getSnackData($mysqli, $maxSnack) {
  return getFoodPortionData($mysqli, $maxSnack, "snack", "inclusioneSnack", "codSnack");
}

/* Returns an array with the keys corrisponding to the drink name
 * and the value corresponding to its sale numbers
 */
function getDrinkData($mysqli, $maxDrink) {
  return getFoodPortionData($mysqli, $maxDrink, "bevanda", "inclusioneBevanda", "codBevanda");
}


 ?>
