<?php

function getSelectText($pagina) {
  switch ($pagina) {
    case 'prodottoStandard':
      return "il/i prodotto/i";
    case 'menu':
      return "il/i menu";
    case 'ingrediente':
      return "l'ingrediente singolo o gli ingredienti";
    case 'bevanda':
      return "la/e bevanda/e";
    case 'snack':
      return "lo/gli snack";
  }
}

function getAllElementsOf($mysqli, $pagina) {
  $sql = "";
  $tab = $pagina;
  switch ($pagina) {
    case 'prodottoStandard':
    case 'menu':
      $sql = "SELECT id, nome AS 'nomeProdotto'
              FROM " . $tab;
      break;
    case 'ingrediente':
      $sql = "SELECT id, nome AS 'nomeProdotto', codTipo
              FROM ingrediente
              ORDER BY codTipo";
      break;
    case 'bevanda':
    case 'snack':
      $sql = "SELECT f.id, f.nome AS 'nomeProdotto', p.nome AS 'porzione'
              FROM " . $tab . " f, porzione p
              WHERE p.id = f.codPorzione";
      break;
    case 'tipoProdotto':
    case 'tipoIngrediente':
    case 'porzione':
      $sql = "SELECT id, nome
              FROM " . $tab;
      break;
  }
  return $mysqli->query($sql);
}

function getIdOf($pagina, $row) {
  $id =  $row["id"];
  if($pagina == "ingrediente") {
    $id .= "_" . $row["codTipo"];
  }
  return $id;
}

function getInsertName($pagina) {
  switch ($pagina) {
    case 'prodottoStandard':
      return "del prodotto";
    case 'menu':
      return "del menu";
    case 'ingrediente':
      return 'dell ingrediente';
    case 'bevanda':
      return "della bevanda";
    case 'snack':
      return "dello snack";
  }
}

 ?>
