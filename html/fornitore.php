<?php

  require 'utils/functions.php';
  require 'utils/db_connect.php';
  require 'func_page/fornitore_func.php';
  sec_session_start();

  $mysqli = connectToDatabase();
  if (!login_check($mysqli)) {
    header('Location: login.php?error=99');
  } else if (isset($_SESSION["email"]) && !isAdmin($_SESSION["email"], $mysqli)) {
    header('Location: index.html');
  }

  if(isset($_POST["operation"])) {
    switch($_POST["operation"]) {
      case "ins":
        addElements($_POST, $mysqli);
        break;
      case "del":
        deleteElements($_POST, $mysqli);
        break;
      case "ins_promo" :
        addPromo($_POST, $mysqli);
        break;
      case "del_promo" :
        removePromo($_POST, $mysqli);
        break;
      case "prep" :
      case "term" :
        sendNotification($_POST["operation"], $_POST, $mysqli);
        break;
      default: echo "error->" . $_POST["operation"] . "<br/>";
    }
  }

  $sql = "SELECT COUNT(id) AS 'conto' FROM notifica WHERE destinatario = 'adminSupremo' AND visto = 0";
  $res = $mysqli->query($sql);
 ?>

<!DOCTYPE html>
<html lang="it-IT">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>Bake&Taste - Portale Fornitore</title>
  <link rel="stylesheet" href="../css/fornitore.css">
  <script src="../script/fornitore.js"></script>
  <script src="../script/Chart.js"></script>
</head>
<body class="bg-light">
  <div class="container-fluid">
    <div class="row" id="dashboard-row">
      <div class="col-md-3" id="menu">
        <nav class="list-group" role="tablist">
          <div class="navbar-brand mx-auto">
            <img src="../Logo/Logo.png" class="img-fluid" alt="">
          </div>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark active" data-toggle="list" id="dash">
            Dashboard
          </button>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark" data-toggle="list" id="ordini">
            Ordini da preparare/consegnare
            <span id="badge"></span>
          </button>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark" data-toggle="list" id="promo">
            Inserisci/Rimuovi promozione
          </button>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark" data-toggle="list" id="ins_prodottoStandard">
            Aggiungi/Rimuovi prodotto
          </button>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark" data-toggle="list" id="ins_menu">
            Aggiungi/Rimuovi menu
          </button>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark" data-toggle="list" id="ins_ingrediente">
            Aggiungi/Rimuovi ingrediente
          </button>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark" data-toggle="list" id="ins_snack">
            Aggiungi/Rimuovi sfiziosità
          </button>
          <button type="button" class="list-group-item list-group-item-action list-group-item-dark" data-toggle="list" id="ins_bevanda">
            Aggiungi/Rimuovi bibita
          </button>
        </nav>
      </div>

      <div class="col-md-9">
        <div class="tab-content" id="dati">
        </div>
      </div>
    </div>
  </div>
</body>
</html>
