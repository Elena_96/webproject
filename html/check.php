<?php

  include 'utils/db_connect.php';
  include 'utils/functions.php';
  sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
  if(isset($_POST['email'], $_POST['p'])) {
     $email = $_POST['email'];
     $password = $_POST['p']; // Recupero la password criptata.
     $mysqli = connectToDatabase();
     $res = login($email, $password, $mysqli);
     $admin = isAdmin($email, $mysqli);
     if($res === 1 && !$admin) {
        // Login eseguito da non fornitore
        setData($mysqli, $email);
        if(isset($_SESSION["lastPage"])) {
          header('Location:'.$_SESSION["lastPage"]);
        } else {
          header('Location: ./offerte.php');
        }
     } else if ($res === 1 && $admin){
       //Login eseguito da fornitore
       setData($mysqli, $email);
       header('Location: ./fornitore.php');
     } else {
        // Login fallito
        header('Location: ./login.php?error='.$res);
     }
  } else if (isset($_POST['email']) && strcmp($_POST['email'],"") == 0) {
      header('Location: ./login.php?error=2');
  } else if (!isset($_POST['p'])) {
      header('Location: ./login.php?error=3');
  } else {
      // Le variabili corrette non sono state inviate a questa pagina dal metodo POST.
      echo 'Invalid Request';
  }

?>
