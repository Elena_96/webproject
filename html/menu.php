<div id="mySidenav" class="sidenav">
  <ul>
    <li <?php if ($_SESSION["lastPage"] == 'offerte.php') { echo 'class="currentPage"';} ?>><a href="offerte.php"><i class="material-icons">whatshot</i>Offerte del giorno</a></li>
    <li <?php if ($_SESSION["lastPage"] == 'prodMenu.php') { echo 'class="currentPage"';} ?>><a href="prodMenu.php"><i class="material-icons">local_offer</i>Le nostre proposte</a></li>
    <li <?php if ($_SESSION["lastPage"] == 'prodotti.php') { echo 'class="currentPage"';} ?>><a href="prodotti.php"><i class="material-icons">restaurant_menu</i>I nostri prodotti</a></li>
    <li <?php if ($_SESSION["lastPage"] == 'specialita.php') { echo 'class="currentPage"';} ?>><a href="specialita.php"><i class="material-icons">brush</i>Crea la tua Specialità</a></li>
    <li <?php if ($_SESSION["lastPage"] == 'sfiziosita.php') { echo 'class="currentPage"';} ?>><a href="sfiziosita.php"><i class="material-icons">local_pizza</i>Sfiziosità</a></li>
    <li <?php if ($_SESSION["lastPage"] == 'bevande.php') { echo 'class="currentPage"';} ?>><a href="bevande.php"><i class="material-icons">local_bar</i>Bevande</a></li>
  </ul>
</div>
