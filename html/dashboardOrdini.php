<?php
  require 'utils/functions.php';
  require 'utils/db_connect.php';
  require 'func_page/dashboardOrdini_func.php';

  class Notifica {
    public $id; //id
    public $mittente; //da chi è stata scatenata la notifica
    public $nomeMittente;
    public $inPreparazione; //se il carrello è in fase di preparazione oppure è da preparare
    public $indirizzo; //la destinazione
    public $prodottiAcquistati; //prodotti acquistati
    public $prodPersAcq; //prodotti personalizzati acquistati

    function __construct($id, $mittente, $nomeMittente, $messaggio, $indirizzo, $prodottiAcquistati, $prodPersAcq) {
      $this->id = $id;
      $this->mittente = $mittente;
      $this->nomeMittente = $nomeMittente;
      $this->inPreparazione = $messaggio === "in preparazione";
      $this->prodottiAcquistati = $prodottiAcquistati;
      $this->indirizzo = $indirizzo;
      $this->prodPersAcq = $prodPersAcq;
    }
  }

  $mysqli = connectToDatabase();
  $sql = "SELECT n.messaggio, u.nome, u.cognome, u.user, n.codCarrello, c.indirizzo, n.id
          FROM notifica n, utente u, carrello c
          WHERE n.destinatario = 'adminSupremo'
          AND n.visto = 0
          AND n.mittente = u.user
          AND c.id = n.codCarrello
          AND c.utente = n.mittente";
  $res = $mysqli->query($sql);
  $arrayNotifiche = array();
  if($res->num_rows > 0) {
    while($row = $res->fetch_assoc()) {
      $notId = $row["id"];
      $user = $row["user"];
      $nomeUtente = $row["nome"] . " " . $row["cognome"];
      $indirizzo = $row["indirizzo"];
      $codCarr = $row["codCarrello"];
      $msg = is_null($row["messaggio"]) ? "" : $row["messaggio"];
      $prodotti = getAllElementsOf($mysqli, "inclusioneMenu", "menu", "codMenu", $user, $codCarr);
      $prodotti = array_merge($prodotti, getAllElementsOf($mysqli, "inclusioneProdStand", "prodottoStandard", "codProdStand", $user, $codCarr));
      $prodotti = array_merge($prodotti, getAllElementsOf($mysqli, "inclusioneSnack", "snack", "codSnack", $user, $codCarr));
      $prodotti = array_merge($prodotti, getAllElementsOf($mysqli, "inclusioneBevanda", "bevanda", "codBevanda", $user, $codCarr));
      array_push($arrayNotifiche, new Notifica($notId, $user, $nomeUtente, $msg, $indirizzo, $prodotti, getAllPaniniPers($mysqli, $user, $codCarr)));
    }
  } else {
    echo $mysqli->error . "<br/>";
  }
 ?>

 <!DOCTYPE HTML>
 <html>
 <head>
 </head>
 <body>
 	<div class="row"> <!-- prima riga -->
 		<div class="col-12">
      <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col"> # </th>
            <th scope="col"> Utente </th>
            <th scope="col"> Indirizzo </th>
            <th scope="col"> Prodotti Acquistati</th>
            <th scope="col"> Stato dell'ordine</th>
            <th class="hideBorders" scope="col"></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $count = count($arrayNotifiche);
            for($i = 0; $i < $count; $i++) {
              $not = $arrayNotifiche[$i];
          ?>
          <tr>
            <th scope="row"> <?php echo $i+1; ?> </td>
            <td> <?php echo $not->nomeMittente?> </td>
            <td> <?php echo $not->indirizzo ?> </td>
            <td>
              <ul>
              <?php
                $prodotti = $not->prodPersAcq;
                $num = count($prodotti);
                for ($j=0; $j < $num; $j++) {
                  $panino = $prodotti[$j];
                ?>
                  <li>Panino Composto da:
                    <ul>
                <?php
                  foreach ($panino as $ing => $qnt) {
                ?>
                      <li>
                        <?php
                        $msg = "";
                        if($qnt > 1) {
                          $msg .= "x" . $qnt . " - ";
                        }
                        $msg .= $ing;
                        echo $msg;
                        ?>
                      </li>
                <?php
                  }
                 ?>
                     </ul>
                  </li>
                <?php
                }
                $prodotti = $not->prodottiAcquistati;
                $num = count($prodotti);
                for($j = 0; $j < $num; $j++) {
                ?>
                  <li><?php echo $prodotti[$j]; ?></li>
                <?php
                }
              ?>
              </ul>
            </td>
            <td>
              <?php
                if(!$not->inPreparazione) {
                  echo "Da preparare";
                } else {
                  echo "In preparazione";
                }
              ?>
            </td>
            <td class="hideBorders">
              <form action="fornitore.php" method="post">
                <input type="hidden" name="destinatario" value=<?php echo '"' . $not->mittente . '"'; ?>>
                <input type="hidden" name="notificaAdmin" value=<?php echo '"' . $not->id . '"'; ?>>
              <?php
                if(!$not->inPreparazione) {
              ?>
                  <input type="hidden" name="operation" value="prep">
                  <input type="submit" role="button" class="btn btn-secondary" value="Prepara ordine">
              <?php
                } else {
              ?>
                  <input type="hidden" name="operation" value="term">
                  <input type="submit" role="button" class="btn btn-secondary" value="Spedisci ordine">
              <?php
                }
              ?>
                </form>
            </td>
          </tr>
          <?php
            }
           ?>
        </tbody>
      </table>
    </div>
 	</div>
 </body>
 </html>
