<?php
function inserisciPanino($mysqli,$myPanino) {
  $email = $_SESSION["email"];
  $format = "col-sm-10 col-10";
  $prezzo = 0;
  $quantitaGiaPresente = 0;

  $cartId = getCartId($mysqli, $email);
  if ($cartId < 0) {
    echo "errore";
  }

  $id = $_POST["idPanino"];
  $quant = $_POST["p".$id];

  if ($quant==0) {
      return "Attenzione! La quantità non può essere uguale a zero.";
  } else {
    $query1 = "SELECT quantita
               FROM inclusioneProdStand
               WHERE codProdStand = ".$id .
             " AND codCarrello = " . $cartId .
             " AND utente = '" . $email . "'";
    $result = $mysqli->query($query1);
    if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
          $quantitaGiaPresente += $row["quantita"];
        }
    }

    if($quantitaGiaPresente > 0) {
      $tot = $quantitaGiaPresente + $quant;
      $stmt = "UPDATE inclusioneProdStand
               SET quantita =" .$tot .
             " WHERE codProdStand = ".$id .
             " AND codCarrello = " . $cartId .
             " AND utente = '" . $email . "'";
    } else {
      $stmt = "INSERT INTO inclusioneProdStand(codProdStand, codcarrello,utente, quantita)
                VALUES ('$id', '$cartId', '$email','$quant')";
    }

    $query = "SELECT prezzo FROM prodottostandard WHERE id = ".$id;
    $result = $mysqli->query($query);

    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $prezzo += $row["prezzo"]*$quant;
      }
    }

    if ($mysqli->query($stmt) == TRUE) {
      updateCartTotal($mysqli, $cartId, $email, $prezzo);
      return "";
    } else {
      return $mysqli->error;
    }
  }
}

include "utils/functions.php";
include "utils/db_connect.php";
sec_session_start();
$_SESSION["lastPage"] = 'prodotti.php';
$myArray = array();

class Food {
  public $id;
  public $quantita;
  public $prezzo;
  public $name;
  public $desc;
  public $img;

  function __construct($id,$name,$desc,$prezzo,$img) {
    $this->id = $id;
    $this->name = $name;
    $this->quantita = 0;
    $this->desc = $desc;
    $this->prezzo= $prezzo;
    $this->img = $img;
  }
}

$conn = connectToDatabase();
$inserito = false;
$msg = "";
if (!empty($_POST['idPanino'])) {
  if (!login_check($conn)) {
    header('Location: login.php?error=99');
  }
  $msg = inserisciPanino($conn,$_POST['idPanino']);
  $inserito = true;
}

$query_sql="SELECT p.nome as nome,p.descrizione as descr,p.id as id,p.prezzo as prezzo, p.img as img FROM prodottostandard p";
$result = $conn->query($query_sql);
  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      $myFood = new Food($row["id"],$row["nome"],$row["descr"],$row["prezzo"],$row["img"]);
       $myArray[]=$myFood;
   }
 }
?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <title>Prodotti</title>
    <?php require 'utils/commons.html'; ?>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/prodotti.css">
    <script src="../script/prodotti.js"></script>
  </head>
  <body>
    <?php require 'navbar.php' ?>
    <section class="container-fluid page" id="content">
      <div>
        <?php
          $format = "col-sm-10 col-10";
          if($inserito && empty($msg)) {
        ?>
        <div class="row msg">
          <?php echo successOccured($format,"Inserimento completato! Il tuo articolo è stato aggiunto al carrello.");?>
        </div>
        <?php
      } else if($inserito){
        ?>
        <div class="row msg">
          <?php echo errorOccured($format,$msg);?>
        </div>
        <?php
          }
         ?>
      </div>
      <header>
        <h1>I nostri prodotti</h1>
        <h2>Prova le nostre succulente creazioni</h2>
      </header>
      <section>
        <?php foreach ($myArray as $key => $value) {?>
          <form action="prodotti.php" method="post" class="row">
            <fieldset class="border rounded col-11 mx-auto">
              <div class="container-fluid">
                <div class="row align-items-center">
                  <div class="immagine col-12 col-md-5">
                      <img src= <?php echo getImgFolder().$value->img?> alt="<?php echo $value->name?>" class="rounded float-left img-fluid">
                  </div>
                  <div class="col-8 offset-1 offset-md-1 col-md-5">
                    <div class="paragraph">
                      <h3><?php echo $value->name ?></h3>
                      <p>
                        <?php echo $value->desc ?></br>
                        Prezzo: <?php echo number_format($value->prezzo,2)?>€
                      </p>
                      <div class="change">
                        <div class="arrow">
                          <img src="../img/arrow_l.png" alt="Diminuzione quantità" class="img-fluid l <?php echo"size".$value->id?>">
                          <input type="text" id="<?php echo "p".$value->id ?>" name = "<?php echo "p".$value->id?>" value="<?php echo $value->quantita?>" readonly/>
                          <img src="../img/arrow_r.png" alt="Aumento quantità" class="img-fluid r <?php echo"size".$value->id?>"><br/>
                          <input type="hidden" name="idPanino" value="<?php echo $value->id?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-1 col-md-1 align-self-end carrello">
                    <input type="image" src=<?php echo '"'.getImgFolder().'addToCart.png"'; ?> alt="Icona aggiungi al carrello">
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        <?php } ?>
      </section>
    </section>
  </body>
</html>
