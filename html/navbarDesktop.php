<!--
Ho usato gli stessi tuoi id nei bottoni quindi funzionano già con le tue pagine user.ph, carrello.php e notifiche.php
- in alcuni di questi ho messo degli style in line, dopo mettili nel css: ci sono per esempio nell'immagine e non so dove altro
- in user.php fai che non si veda la parte dell'icona dell'utente e del nome quando si è in modalità desktop
- c'è da aggiungere lo sfondo alla navbar se no con il fatto che ha sfondo invisibile, dato che l'ho messa che rimane in alto si
  sovrappone a tutto il resto
- nel css tutte le cose che hai ora per il nav devono essere relative solo al nav che hai creato tu, quindi dovrai metterci davanti
  l'id del tuo nav
-->
<nav class="row navbar sticky-top navbar-expand-lg navbar-light" id="navbar-desktop">
  <a class="navbar-brand">
    <img src="../Logo/Logo.png" width="30" height="30" class="d-inline-block align-top" alt="logo del sito">
    Bake&Taste
  </a>
  <ul class="navbar-nav mr-auto">
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "offerte.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="offerte.php">Offerte del giorno</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "prodmenu.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="prodmenu.php">Le nostre proposte</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "prodotti.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="prodotti.php">I nostri prodotti</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "specialita.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="specialita.php">Crea la tua specialita</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "sfiziosita.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="sfiziosita.php">Sfiziosita</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "bevande.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="bevande.php">Bevande</a>
    </li>
  </ul>
  <ul class="navbar-nav">
    <li class="nav-item">
      <?php
        $mysqli = connectToDatabase();
        if(login_check($mysqli)) {
      ?>
      <a class="nav-link" data-toggle="collapse" href="#utente" role="button" aria-expanded="false" aria-controls="utente">
        <span class="fa fa-user"></span> <?php echo $_SESSION["utente"]; ?> </a>
      </a>
      <?php
        } else {
      ?>
       <a class="nav-link" href="login.php"><span class="fa fa-user"></span> Login </a>
      <?php
        }
      ?>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#notifiche" role="button" aria-expanded="false" aria-controls="notifiche" <?php if (empty($Notifiche)){echo"style='display:none'"; } ?>>
        <span class="fa fa-bell"></span> Notifiche </a>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#carrello" role="button" aria-expanded="false" aria-controls="carrello">
        <span class="fa fa-shopping-cart"></span> Carrello </a>
      </a>
    </li>
  </ul>
</nav>
