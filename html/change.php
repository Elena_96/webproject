<?php
  include 'utils/db_connect.php';
  include 'utils/functions.php';
  sec_session_start();
  $conn=connectToDatabase();
  $login=login_check($conn);
  if ($login) {
    if(isset($_POST['telefono'])) {
      $sql="UPDATE utente
            SET numeroTelefono=".$_POST['telefono']."
            WHERE user='".$_SESSION["email"]."'";
      $conn->query($sql);
    }
    if (isset($_POST['indirizzo'])) {
      $sql='SELECT *
            FROM consegna
            WHERE utente="'.$_SESSION["email"].'"';
      $indirizzo = $conn->query($sql);
      if ($indirizzo->num_rows > 0) {
        $sql="UPDATE consegna
              SET indirizzo='".$_POST['indirizzo']."'
              WHERE utente='".$_SESSION['email']."'";
        $conn->query($sql);
      } else {
        $sql="INSERT INTO consegna(utente, indirizzo)
              VALUES ('".$_SESSION['email']."','".$_POST['indirizzo']."')";
        $conn->query($sql);
      }
    }
    if (isset($_POST['intestatario']) && isset($_POST['numerocarta'])) {
      $sql='SELECT *
            FROM cartacredito
            WHERE utente="'.$_SESSION["email"].'"';
      $indirizzo = $conn->query($sql);
      if ($indirizzo->num_rows > 0) {
        $sql="UPDATE cartacredito
              SET proprietario='".$_POST['intestatario']."', numero=".$_POST['numerocarta']."
              WHERE utente='".$_SESSION["email"]."'";
        $conn->query($sql);
      } else {
        $sql="INSERT INTO cartacredito(utente, proprietario, numero)
              VALUES ('".$_SESSION['email']."','".$_POST['intestatario']."',".$_POST['numerocarta'].")";
        $conn->query($sql);
      }
    }
    if (isset($_POST['p'])) {
      $password = $_POST['p'];
      $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
      // Crea una password usando la chiave appena creata.
      $password = hash('sha512', $password.$random_salt);
      // Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
      // Assicurati di usare statement SQL 'prepared'.
      if ($insert_stmt = $conn->prepare("UPDATE utente SET pwd=? , codice=? WHERE user=?")) {
        $insert_stmt->bind_param('sss', $password, $random_salt, $_SESSION["email"]);
        // Esegui la query ottenuta.
        $insert_stmt->execute();
      }
      login($_SESSION["email"], $_POST["p"], $conn);
      $conn->close();
    }
    header('Location: '.$_SESSION["lastPage"]);
  } else {
    header('Location: '.$_SESSION["lastPage"]);
  }
?>
