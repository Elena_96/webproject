<?php
    include 'utils/functions.php';
    include 'utils/db_connect.php';
    require 'utils/commons.html';
    sec_session_start();
    $conn=connectToDatabase();
    $login=login_check($conn);
    if ($login) {
      $sql='SELECT u.nome as nome, u.cognome as cognome, u.sesso as sesso, u.dataNascita as data, u.numeroTelefono as telefono, u.fotoProfilo as foto
         FROM utente u
         WHERE u.user="'.$_SESSION["email"].'"';
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
        $data = $result->fetch_assoc();
      }

      $sql='SELECT indirizzo
         FROM consegna
         WHERE utente="'.$_SESSION["email"].'"';
      $indirizzo = $conn->query($sql);
      if ($indirizzo->num_rows > 0) {
        $ind = $indirizzo->fetch_assoc();
      }

      $sql="SELECT numero, proprietario
         FROM cartacredito
         WHERE utente='".$_SESSION["email"]."'";
      $carta = $conn->query($sql);
      if ($carta->num_rows > 0) {
        $card = $carta->fetch_assoc();
      }
    } else {
      header("Location: ".$_SESSION["lastPage"]);
    }

 ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dati Personali</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Boogaloo|Fjalla+One|Leckerli+One" rel="stylesheet">
    <link rel="stylesheet" href="../css/data.css">
    <link rel="stylesheet" href="../css/switchery.css">
    <script src="../script/sha512.js"> </script>
    <script src="../script/switchery.js"></script>
    <script src="../script/data.js"></script>
    <script src="../script/formData.js"> </script>
  </head>
  <body class="container-fluid">
    <header class="container-fluid">
      <div class="mx-auto">
        <img src="../Logo/Logo.png" alt="logo del sito" class="img-fluid"/>
      </div>
    <h1>Dati Personali</h1>
    </header>
    <form id="form" class="mx-auto" action="./change.php" method="post">
      <fieldset>
        <legend>Generalit&agrave;</legend>
          <label >Nome: <?php echo ($data["nome"]) ?></label></br>
          <label>Cognome: <?php echo ($data["cognome"]) ?></label></br>
          <label>Data di nascita: <?php echo ($data["data"]) ?></label></br>
          <label>Sesso: <?php switch ($data["sesso"]) { case "M": echo ("Uomo"); break; case "F": echo ("Donna"); break; case "A": echo ("Altro"); break; } ?></label></br>
          <label>Email: <?php echo ($_SESSION["email"]) ?></label></br>
          <label for="telefono">Telefono</label><br/>
          <input id="telefono"  type="tel" name="telefono" value="<?php echo ($data["telefono"]) ?>" required><br/>
      </fieldset>

      <fieldset>
        <legend>Indirizzo di consegna preferito</legend>
        <label for="indirizzo">Indirizzo (Cesena)</label><br/>
        <input id="indirizzo" type="text" name="indirizzo" placeholder="Indirizzo" <?php if ($indirizzo->num_rows>0){ echo ('value="'.$ind["indirizzo"].'"'); }?>><br/>
      </fieldset>

      <fieldset>
        <legend>Metodo di pagamento</legend>
        <label for="intestatario">Intestatario*</label><br/>
        <input id="intestatario" type="text" name="intestatario" placeholder="Intestatario" <?php if ($carta->num_rows>0){ echo ('value="'.$card["proprietario"].'"'); }?>><br/>
        <label for="numerocarta">Numero della carta*</label><br/>
        <input id="numerocarta" type="tel" name="numerocarta" placeholder="Numero della carta" <?php if ($carta->num_rows>0){ echo ('value="'.$card["numero"].'"'); }?>><br/>
        <p>(* I campi sono opzionali.)</p>
      </fieldset>

      <div class="switch1">
        <input id="switchery" type="checkbox" name="switchery" class="switch" value=0 /> Vuoi cambiare la Password?
      </div>
      <fieldset class="password">
				<legend>Cambio Password</legend>
    				<label for="pwd">Nuova Password</label><br/>
    				<input id="pwd" type="password" name="pwd" placeholder="Nuova Password"/><br/>
            <label for="pwdbis">Ripeti la Nuova Password</label><br/>
    				<input id="pwdbis" type="password" name="pwdbis" placeholder="Nuova Password"/><br/>
  	  </fieldset>
      <div>
        <input type="submit" class="button" value="Salva modifiche" onclick="return formhash('form');"/>
      </div>
    </form>
  </body>
</html>
