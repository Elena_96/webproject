<div class="collapse navbar-collapse" id="utente">
  <section>
    <?php if ($login) { ?>
      <i class="fa fa-user" style="font-size:100px"></i>
      <h1><?php echo $_SESSION["utente"] ?> </h1>
    <?php } else { ?>
      <i class="fa fa-user-secret" style="font-size:100px"></i>
      <h1> Anonimo </h1>
    <?php } ?>
  </section>
  <section>
    <ul>
      <?php if ($login) { ?>
        <li><a href="data.php" class="button">Dati personali</a></li>
        <li><a href="acquisti.php" class="button">I miei acquisti</a></li>
        <li>
          <form action="" method="post">
            <input type="hidden" name="logout" value="true" />
            <button>Logout</button>
          </form>
        </li>
      <?php } else { ?>
        <li><a href="logIn.php" class="button">Login</a></li>
        <li><a href="signIn.php" class="button">Registrati</a></li>
      <?php } ?>
    </ul>
  </section>
</div>
