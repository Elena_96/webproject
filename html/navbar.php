<?php
  check_logout();

  class CartItem
  {
    public $id;
    public $quantita;
    public $prezzo;
    public $nome;
    public $porzione;
    public $sconto;
    public $descrizione;

    function __construct($id,$nome,$prezzo,$sconto,$quantita,$descrizione,$porzione,$tipologia)
    {
      $this->id = $tipologia."-".$id;
      $this->nome = $nome;
      $this->quantita = $quantita;
      $this->porzione = $porzione;
      $this->prezzo = $prezzo;
      $this->sconto = $sconto;
      $this->descrizione = $descrizione;
    }
  }
  class Notifica
  {
    public $id;
    public $msg;
    public $CartId;

    function __construct($id,$msg,$CartId)
    {
      $this->id = $id;
      $this->msg = $msg;
      $this->CartId = $CartId;
    }
  }

  $conn=connectToDatabase();
  $login=login_check($conn);
  $ItemInCart=array();
  $Notifiche=array();
  if ($login) {
    $CartId=getCartId($conn, $_SESSION['email']);

    $query_sql="SELECT id, codCarrello, messaggio
                FROM notifica
                WHERE destinatario='".$_SESSION['email']."' AND visto=0" ;

    $result = $conn->query($query_sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $noti = new Notifica($row["id"],$row["messaggio"],$row["codCarrello"]);
        $Notifiche[]=$noti;
      }
    }

    $query_sql="SELECT m.nome as nome, i.quantita as quantita, m.id as id,m.prezzo as prezzo, m.sconto as sconto
                FROM inclusionemenu i, menu m
                WHERE i.codcarrello = ".$CartId." AND i.codmenu = m.id AND i.utente='".$_SESSION['email']."'" ;

    $result = $conn->query($query_sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $myFood = new CartItem($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],"","","menu");
        $ItemInCart[]=$myFood;
      }
    }

     $query_sql="SELECT p.nome as nome, i.quantita as quantita, p.id as id,p.prezzo as prezzo, p.sconto as sconto, p.descrizione as descrizione
                 FROM inclusioneprodstand i, prodottostandard p
                 WHERE i.codcarrello = ".$CartId." AND i.codProdStand = p.id AND i.utente='".$_SESSION['email']."'" ;

     $result = $conn->query($query_sql);
     if ($result->num_rows > 0) {
       while($row = $result->fetch_assoc()) {
         $myFood = new CartItem($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],$row["descrizione"],"","prodStand");
          $ItemInCart[]=$myFood;
        }
      }

      $query_sql="SELECT p.id as id, p.prezzofinale as prezzo, t.nome as nome
                  FROM prodottopersonalizzato p, tipoprodotto t
                  WHERE p.codcarrello = ".$CartId." AND p.codTipo = t.id AND p.utente='".$_SESSION['email']."'" ;

      $result = $conn->query($query_sql);
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $query_sql2="SELECT i.nome as nome, t.quantita as quantita
                      FROM inclusioneingredienti t, ingrediente i
                      WHERE t.codProdPers=".$row["id"]." AND t.codIngr = i.id AND t.tipoIngr = i.codTipo
                      ORDER BY i.codTipo" ;
          $result2 = $conn->query($query_sql2);
          $Ingr="Ingredienti:";
          $i=0;
          while($row2 = $result2->fetch_assoc()) {
            $i=$i+1;
            $Ingr=$Ingr." ".$row2["nome"];
            if ($row2['quantita']>1) {
              $Ingr=$Ingr."x".$row2["quantita"];
            }
            if ($i<$result2->num_rows) {
              $Ingr=$Ingr.",";
            }
          }
          $myFood = new CartItem($row["id"],$row["nome"]." pers.",$row["prezzo"],"",1,$Ingr,"","prodPers");
           $ItemInCart[]=$myFood;
         }
       }

      $query_sql="SELECT s.nome as nome, i.quantita as quantita, s.id as id,s.prezzo as prezzo, s.sconto as sconto, p.nome as porzione
                  FROM inclusionesnack i, snack s, porzione p
                  WHERE i.codcarrello = ".$CartId." AND i.codsnack = s.id AND  p.id = s.codPorzione AND i.utente='".$_SESSION['email']."'";

      $result = $conn->query($query_sql);
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $myFood = new CartItem($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],"",$row["porzione"],"snack");
           $ItemInCart[]=$myFood;
         }
       }

       $query_sql="SELECT b.nome as nome, i.quantita as quantita, b.id as id,b.prezzo as prezzo, b.sconto as sconto, p.nome as porzione
                   FROM inclusionebevanda i, bevanda b, porzione p
                   WHERE i.codcarrello = ".$CartId." AND i.codbevanda = b.id AND  p.id = b.codPorzione AND i.utente='".$_SESSION['email']."'";

       $result = $conn->query($query_sql);
       if ($result->num_rows > 0) {
         while($row = $result->fetch_assoc()) {
           $myFood = new CartItem($row["id"],$row["nome"],$row["prezzo"],$row["sconto"],$row["quantita"],"",$row["porzione"],"bevanda");
            $ItemInCart[]=$myFood;
          }
        }
      }
 ?>


<nav class="row fixed-top" id="navbar-mobile">
  <div class="col-6">
    <img src="../img/menu.png" alt="Menù" onclick="openClose()">
  </div>
  <div class="col-6">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#utente" id="user_button">
      <img src="../img/user.png" alt="Utente"/>
    </button>
    <button class="navbar-toggler" type="button" data-toggle="collapse" id="notify_button" data-target="#notifiche" <?php if (empty($Notifiche)){echo"style='display:none'"; } ?>>
      <img src="../img/bell.png" alt="Notifiche">
    </button>
    <button class="navbar-toggler" type="button" data-toggle="collapse" id="cart_button" data-target="#carrello">
      <img src="../img/cart.png" alt="Carrello">
    </button>
  </div>
</nav>
<nav class="row navbar fixed-top navbar-expand-lg navbar-light" id="navbar-desktop">
  <a class="navbar-brand">
    <img src="../Logo/Logo.png" width="30" height="30" class="d-inline-block align-top" alt="logo del sito">
    Bake&Taste
  </a>
  <ul class="navbar-nav mr-auto">
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "offerte.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="offerte.php">Offerte del giorno</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "prodMenu.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="prodMenu.php">Le nostre proposte</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "prodotti.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="prodotti.php">I nostri prodotti</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "specialita.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="specialita.php">Crea la tua specialita</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "sfiziosita.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="sfiziosita.php">Sfiziosita</a>
    </li>
    <li
      <?php
        $str = 'class="nav-item';
        if($_SESSION["lastPage"] == "bevande.php"){
          $str .= " active";
        }
        echo $str . '"';
      ?>>
      <a class="nav-link" href="bevande.php">Bevande</a>
    </li>
  </ul>
  <ul class="navbar-nav">
    <li class="nav-item">
      <?php
        if($login) {
      ?>
      <button class="nav-link" data-toggle="collapse" href="#utente" role="button" id="user_button" aria-expanded="false" aria-controls="utente">
        <img src="../img/user.png" alt="Utente"/><!--<span class="fa fa-user"></span>--> <?php echo $_SESSION["utente"]; ?>
      </a>
      <?php
        } else {
      ?>
       <a class="nav-link" href="login.php"><img src="../img/user.png" alt="Utente"/><!--<span class="fa fa-user"></span>--> Login </a>
      <?php
        }
      ?>
    </li>
    <li class="nav-item">
      <button class="nav-link" data-toggle="collapse" href="#notifiche" id="notify_button" aria-expanded="false" aria-controls="notifiche" <?php if (empty($Notifiche)){echo"style='display:none'"; } ?>>
        <img src="../img/bell.png" alt="Notifiche"><!--<span class="fa fa-bell"></span>--> Notifiche
      </button>
    </li>
    <li class="nav-item">
      <button class="nav-link" data-toggle="collapse" href="#carrello" id="cart_button" aria-expanded="false" aria-controls="carrello">
        <img src="../img/cart.png" alt="Carrello"><!--<span class="fa fa-shopping-cart"></span>--> Carrello
      </button>
    </li>
  </ul>
</nav>

<div class="tendine">
  <?php
  require 'user.php';
  require 'notifiche.php';
  require 'carrello.php';
  ?>
</div>
<?php
require 'menu.php';
?>
